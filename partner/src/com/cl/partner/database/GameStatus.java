package com.cl.partner.database;

public class GameStatus {
	public static final int NONE = 1; //未下载
	public static final int DOWNLOADING = 2; //下载中
	public static final int SUCCESS = 3; //下载成功
	public static final int FAIL = 4; //下载失败
}
