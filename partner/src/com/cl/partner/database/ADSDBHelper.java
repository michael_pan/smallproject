package com.cl.partner.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cl.partner.bean.ResGame;

/**
 * @ClassName: ADSDBHelper
 * @Description: ADSDBHelper
 */

public class ADSDBHelper {
	private static final String TAG = "ADSDBHelper";
	// Define for DB
	private static final String DB_NAME = "ads.db"; // DB's name
	private static final String DB_TABLE = "AD"; // DB's TABLE name
	private static final int DB_VERSION = 1; // DB's Version
	//
	private static final String ID = "_id";
	private static final String PACKAGENAME = "pkgname";
	private static final String NAME = "name";
	private static final String URL = "url";
	private static final String ICONURL = "iconurl";
	private static final String SIZE = "size";
	private static final String SIZEBYTE = "sizebyte";
	private static final String DESC = "content";
	private static final String VERSION = "version";
	private static final String VERSIONCODE = "versioncode";
	private static final String CLASSNAME = "classname";
	private static final String DLCOUNT = "dlcount";
	// cmd string for create table
	private static final String SQL_CREATE_TABLE = "CREATE TABLE " + DB_TABLE
			+ " (" + ID + " INTEGER primary key AUTOINCREMENT," + PACKAGENAME
			+ " TEXT," + NAME + " TEXT," + URL + " TEXT," + ICONURL + " TEXT,"
			+ SIZE + " TEXT," + SIZEBYTE + " TEXT," + DESC + " TEXT," + VERSION
			+ " TEXT," + VERSIONCODE + " TEXT," + CLASSNAME + " TEXT,"
			+ DLCOUNT + " TEXT)";

	private static final String SQL_DELETE_TABLE = "drop table if exists "
			+ PACKAGENAME;

	private Context mContext = null;
	private SQLiteDatabase mSQLiteDatabase = null;
	private DatabaseHelper mDatabaseHelper = null;

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(SQL_CREATE_TABLE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL(SQL_DELETE_TABLE);
			db.execSQL(SQL_CREATE_TABLE);
		}
	}

	public ADSDBHelper(Context context) {
		mContext = context;
	}

	// open db
	public void open() {
		try {
			mDatabaseHelper = new DatabaseHelper(mContext);
			mSQLiteDatabase = mDatabaseHelper.getWritableDatabase();
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "e = "+e);
		}
	}

	// close db
	public void close() {

		if (mSQLiteDatabase != null) {
			mSQLiteDatabase.close();
			mSQLiteDatabase = null;
		}

		if (mDatabaseHelper != null) {
			mDatabaseHelper.close();
			mDatabaseHelper = null;
		}
	}

	// beginTransaction
	private void beginTransaction() {
		mSQLiteDatabase.beginTransaction();
	}

	// endTransaction
	private void endTransaction() {
		mSQLiteDatabase.setTransactionSuccessful();
		mSQLiteDatabase.endTransaction();
	}

	// delete all rows
	private void deleteAll() {
		if (mSQLiteDatabase == null)
			return;
		mSQLiteDatabase.delete(DB_TABLE, null, null);
	}

	// insert all
	public void insertAll(ArrayList<ResGame> list) {
		if (mSQLiteDatabase == null)
			return;
		if (list == null || list.size() < 1)
			return;
		deleteAll();
		beginTransaction();
		for (ResGame item : list) {
			insertData(item);
		}
		endTransaction();
	}

	public ArrayList<ResGame> queryAll() {
		ArrayList<ResGame> list = new ArrayList<ResGame>();
		if (mSQLiteDatabase == null)
			return list;
		String[] columns = new String[] { PACKAGENAME, NAME, URL, ICONURL,
				SIZE, SIZEBYTE, DESC, VERSION, VERSIONCODE, CLASSNAME, DLCOUNT };
		Cursor cursor = mSQLiteDatabase.query(DB_TABLE, columns, null, null,
				null, null, null);
		if (cursor != null) {
			while (cursor.moveToNext()) {
				ResGame item = new ResGame();
				item.gamePkgName = cursor.getString(0);
				item.gameName = cursor.getString(1);
				item.gameDownloadUrl = cursor.getString(2);
				item.gameIconUrl = cursor.getString(3);
				item.gameSizeNick = cursor.getString(4);
				item.gameSizeByte = cursor.getLong(5);
				item.gameDesc = cursor.getString(6);
				item.gameVer = cursor.getString(7);
				item.gameVerInt = cursor.getInt(8);
				item.gameClassName = cursor.getString(9);
				item.gameDownloadCountNick = cursor.getString(10);
				list.add(item);
			}
			cursor.close();
		}
		return list;
	}

	// insert a data
	public long insertData(ResGame item) {
		ContentValues values = new ContentValues();
		values.put(PACKAGENAME, item.gamePkgName);
		values.put(NAME, item.gameName);
		values.put(URL, item.gameDownloadUrl);
		values.put(ICONURL, item.gameIconUrl);
		values.put(SIZE, item.gameSizeNick);
		values.put(SIZEBYTE, item.gameSizeByte);
		values.put(DESC, item.gameDesc);
		values.put(VERSION, item.gameVer);
		values.put(VERSIONCODE, item.gameVerInt);
		values.put(CLASSNAME, item.gameClassName);
		values.put(DLCOUNT, item.gameDownloadCountNick);
		return mSQLiteDatabase.insert(DB_TABLE, null, values);
	}
}
