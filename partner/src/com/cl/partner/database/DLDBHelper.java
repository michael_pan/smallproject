package com.cl.partner.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @ClassName: DLDBHelper
 * @Description: DLDBHelper
 */

public class DLDBHelper {
	private static final String TAG = "DLDBHelper";
	// Define for DB
	private static final String DB_NAME = "ads.db"; // DB's name
	private static final String DB_TABLE = "AD"; // DB's TABLE name
	private static final int DB_VERSION = 1; // DB's Version
	//
	private static final String ID = "_id";
	private static final String PACKAGENAME = "pkgname";
	private static final String NAME = "name";
	private static final String URL = "url";
	private static final String ICONURL = "iconurl";
	private static final String SIZE = "size";
	private static final String SIZEBYTE = "sizebyte";
	private static final String DESC = "content";
	private static final String VERSION = "version";
	private static final String VERSIONCODE = "versioncode";
	private static final String CLASSNAME = "classname";
	private static final String DLCOUNT = "dlcount";
	// cmd string for create table
	private static final String SQL_CREATE_TABLE = "CREATE TABLE " + DB_TABLE
			+ " (" + ID + " INTEGER primary key AUTOINCREMENT," + PACKAGENAME
			+ " TEXT," + NAME + " TEXT," + URL + " TEXT," + ICONURL + " TEXT,"
			+ SIZE + " TEXT," + SIZEBYTE + " TEXT," + DESC + " TEXT," + VERSION
			+ " TEXT," + VERSIONCODE + " TEXT," + CLASSNAME + " TEXT,"
			+ DLCOUNT + " TEXT)";

	private static final String SQL_DELETE_TABLE = "drop table if exists "
			+ PACKAGENAME;

	private Context mContext = null;
	private SQLiteDatabase mSQLiteDatabase = null;
	private DatabaseHelper mDatabaseHelper = null;

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(SQL_CREATE_TABLE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL(SQL_DELETE_TABLE);
			db.execSQL(SQL_CREATE_TABLE);
		}
	}

	public DLDBHelper(Context context) {
		mContext = context;
	}

	// open db
	public void open() {
		try {
			mDatabaseHelper = new DatabaseHelper(mContext);
			mSQLiteDatabase = mDatabaseHelper.getWritableDatabase();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// close db
	public void close() {

		if (mSQLiteDatabase != null) {
			mSQLiteDatabase.close();
			mSQLiteDatabase = null;
		}

		if (mDatabaseHelper != null) {
			mDatabaseHelper.close();
			mDatabaseHelper = null;
		}
	}

	// beginTransaction
	public void beginTransaction() {
		mSQLiteDatabase.beginTransaction();
	}

	// endTransaction
	public void endTransaction() {
		mSQLiteDatabase.setTransactionSuccessful();
		mSQLiteDatabase.endTransaction();
	}

	// insert a data
	// public long insertData(String packageName, String appName, Bitmap icon,
	// String version, int versionCode, String dataDir, long modifiedTime,
	// long apkSize) {
	// ContentValues initialValues = initContentValues(packageName, appName,
	// icon, version, versionCode, dataDir, modifiedTime, apkSize);
	// return mSQLiteDatabase.insert(DB_TABLE, null, initialValues);
	// }

	// insert a data
	// public long insertData(ApkInfo apkInfo) {
	//
	// ContentValues initialValues = initContentValues(apkInfo.packageName,
	// apkInfo.appName,
	// apkInfo.icon, apkInfo.version, apkInfo.versionCode,
	// apkInfo.dataDir, apkInfo.modifiedTime, apkInfo.apkSize);
	//
	// return mSQLiteDatabase.insert(DB_TABLE, null, initialValues);
	// }

	// delete a data
	// public boolean deleteData(String packageName) {
	// String where = PACKAGE_NAME + " = ?";
	// String[] whereValue = { packageName };
	// return mSQLiteDatabase.delete(DB_TABLE, where, whereValue) > 0;
	// }
	//
	// // fetch all data
	// public Cursor fetchAllData(int sortType) {
	// String[] columns = new String[] { PACKAGE_NAME, APP_NAME, ICON,
	// VERSION, VERSION_CODE, DATA_DIR, MODIFIED_TIME, APK_SIZE };
	// String sort = MODIFIED_TIME;
	// if (sortType == Define.SORT_BY_TIME_DESC) {
	// sort = MODIFIED_TIME + " DESC";
	// } else if (sortType == Define.SORT_BY_TIME_ASC) {
	// sort = MODIFIED_TIME + " ASC";
	// } else if (sortType == Define.SORT_BY_NAME_DESC) {
	// sort = APP_NAME + " DESC";
	// } else if (sortType == Define.SORT_BY_NAME_ASC) {
	// sort = APP_NAME + " ASC";
	// }
	// return mSQLiteDatabase.query(DB_TABLE, columns, null, null, null, null,
	// sort);
	// }
	//
	// // query a data by package's name
	// public Cursor fetchData(String packageName) throws SQLException {
	// String where = PACKAGE_NAME + " = ?";
	// String[] whereValue = { packageName };
	// String[] columns = new String[] { PACKAGE_NAME, APP_NAME, ICON,
	// VERSION, VERSION_CODE, DATA_DIR, MODIFIED_TIME, APK_SIZE };
	// Cursor mCursor = mSQLiteDatabase.query(true, DB_TABLE, columns, where,
	// whereValue, null, null, null, null);
	// return mCursor;
	//
	// }
	//
	// // update data by package's name
	// public boolean updateData(String packageName, String appName, Bitmap
	// icon,
	// String version, int versionCode, String dataDir, long modifiedTime,
	// long apkSize) {
	//
	// ContentValues initialValues = initContentValues(packageName, appName,
	// icon, version, versionCode, dataDir, modifiedTime, apkSize);
	//
	// String where = PACKAGE_NAME + " = ?";
	// String[] whereValue = { packageName };
	// return mSQLiteDatabase.update(DB_TABLE, initialValues, where,
	// whereValue) > 0;
	// }
	//
	// private ContentValues initContentValues(String packageName, String
	// appName,
	// Bitmap icon, String version, int versionCode, String dataDir,
	// long modifiedTime, long apkSize) {
	//
	// ContentValues initialValues = new ContentValues();
	// initialValues.put(PACKAGE_NAME, packageName);
	// initialValues.put(APP_NAME, appName);
	//
	// initialValues.put(VERSION, version);
	// initialValues.put(VERSION_CODE, versionCode);
	// initialValues.put(DATA_DIR, dataDir);
	// initialValues.put(MODIFIED_TIME, modifiedTime);
	// initialValues.put(APK_SIZE, apkSize);
	// return initialValues;
	// }

}
