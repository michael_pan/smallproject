package com.cl.partner.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cl.partner.R;
import com.cl.partner.bean.ResGame;
import com.cl.partner.database.GameStatus;
import com.cl.partner.download.DownloadManagerPro;
import com.cl.partner.picmgr.PicMgr;
import com.cl.partner.utils.UIUtils;

/**
 * @ClassName: GameListAdapter
 * @Description: 应用列表Adapter
 */

public class GameListAdapter extends BaseAdapter {
	private static final String TAG = "GameListAdapter";
	private ArrayList<ResGame> games;
	private DownloadManagerPro dm;
	private Context ctx;

	public GameListAdapter(Context ctx, DownloadManagerPro dm) {
		this.ctx = ctx;
		this.games = new ArrayList<ResGame>();
		this.dm = dm;

	}

	@Override
	public int getCount() {
		return games.size();
	}

	@Override
	public Object getItem(int position) {
		return games.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void updateGames(ArrayList<ResGame> games) {
		this.games = games;
		notifyDataSetChanged();
	}

	
	public long OnItemClick(int position) {
		if (position < 0 || position >= games.size())
			return -1;
		ResGame game = games.get(position);
		if (game.downloadID <= 0) {
			game.downloadID = dm.startDownload(game.gameDownloadUrl, game.gameName,
					game.gameDesc);
			return game.downloadID;
		} else if (game.gameStatus == GameStatus.SUCCESS) {
			String localuri = dm.getLocalUri(game.downloadID);
			Log.e(TAG, "localuri = " + localuri);
			UIUtils.gotoIntalllUri(ctx, localuri);
			return -1;
		} else {
			return -1;
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(ctx).inflate(
					R.layout.ad_list_item, null);
			holder = new ViewHolder();
			holder.icon = (ImageView) convertView.findViewById(R.id.ad_icon);
			holder.name = (TextView) convertView.findViewById(R.id.ad_name);
			holder.size = (TextView) convertView.findViewById(R.id.ad_size);
			holder.count = (TextView) convertView.findViewById(R.id.ad_count);
			holder.content = (TextView) convertView
					.findViewById(R.id.ad_content);

			holder.download_tv = (TextView) convertView
					.findViewById(R.id.ad_download_tv);
			holder.download_iv = (ImageView) convertView
					.findViewById(R.id.ad_download_icon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		ResGame game = games.get(position);
		holder.name.setText(String.valueOf(position + 1) + "." + game.gameName);
		holder.size.setText("大小：" + game.gameSizeNick);
		holder.count.setText("下载 : " + game.gameDownloadCountNick);
		holder.content.setText(game.gameDesc);
		PicMgr.getInstance().loadBitmap(holder.icon,game.gameIconUrl,R.drawable.icon_def);
		if (game.gameStatus == GameStatus.DOWNLOADING) {
			holder.download_tv.setText(game.gamePrecent + "%");
			holder.download_iv
					.setBackgroundResource(R.drawable.ad_btn_download_bg);
		} else if (game.gameStatus == GameStatus.SUCCESS) {
			holder.download_tv.setText(R.string.ad_success);
			holder.download_iv
					.setBackgroundResource(R.drawable.ad_btn_install_bg);
		} else {
			holder.download_tv.setText(R.string.ad_download);
			holder.download_iv
					.setBackgroundResource(R.drawable.ad_btn_download_bg);
		}
		return convertView;
	}

	

	static class ViewHolder {
		ImageView icon;
		TextView name;
		TextView size;
		TextView count;
		TextView content;
		ImageView download_iv;
		TextView download_tv;
	}

	public void updateView(long id, int pos, int status, int precent) {
		if (pos < 0 || pos >= games.size())
			return;
		ResGame game = games.get(pos);
		game.gameStatus = status;
		game.gamePrecent = precent;
		game.downloadID = id;
		notifyDataSetChanged();
	}

}
