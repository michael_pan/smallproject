package com.cl.partner.widget.ActionBar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cl.partner.R;

/**
 * @ClassName: ActionBar
 * @Description: ActionBar
 */
public class ActionBar extends RelativeLayout {

	private static final String TAG = ActionBar.class.getSimpleName();
	public static final int BACK = 1000;
	public static final int BTN1 = 1001;
	public static final int BTN2 = 1002;
	public static final int BTN3 = 1003;
	public static final int BTN4 = 1004;

	// 标题和返回
	private LinearLayout backLy;
	private ImageView backIV;
	private ImageView iconIV;
	private TextView titleTV;
	// 右边BTN
	private LinearLayout btnly1;
	private LinearLayout btnly2;
	private LinearLayout btnly3;
	private LinearLayout btnly4;
	private ImageView btn1;
	private ImageView btn2;
	private ImageView btn3;
	private ImageView btn4;

	// 点击监听
	private OnActionBarBtnClick clickListener;

	/**
	 * @param context
	 */
	public ActionBar(Context context) {
		super(context);
		init(context);
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public ActionBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public ActionBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.ad_actionbar, this);
		backLy = (LinearLayout) findViewById(R.id.actionbar_back_ly);
		titleTV = (TextView) findViewById(R.id.actionbar_title_tv);
		iconIV = (ImageView) findViewById(R.id.actionbar_icon_btn);
		backIV = (ImageView) findViewById(R.id.actionbar_back_btn);
		btnly1 = (LinearLayout) findViewById(R.id.actionbar_ly1);
		btnly2 = (LinearLayout) findViewById(R.id.actionbar_ly2);
		btnly3 = (LinearLayout) findViewById(R.id.actionbar_ly3);
		btnly4 = (LinearLayout) findViewById(R.id.actionbar_ly4);
		btn1 = (ImageView) findViewById(R.id.actionbar_btn1);
		btn2 = (ImageView) findViewById(R.id.actionbar_btn2);
		btn3 = (ImageView) findViewById(R.id.actionbar_btn3);
		btn4 = (ImageView) findViewById(R.id.actionbar_btn4);
	}

	// set onclick listener
	public void setOnclickListener(OnActionBarBtnClick listener) {
		this.clickListener = listener;
		setListener();
	}

	// set title
	public void setTitle(int titleID, int iconID) {
		if (titleID <= 0 || iconID <= 0)
			return;
		titleTV.setText(titleID);
		titleTV.setVisibility(View.VISIBLE);
		iconIV.setBackgroundResource(iconID);
		iconIV.setVisibility(View.VISIBLE);
	}

	// enable back
	public void showBackBtn(boolean isShow) {
		int visable = isShow ? View.VISIBLE : View.INVISIBLE;
		backIV.setVisibility(visable);
		backLy.setClickable(isShow);
	}

	// set Function Buttons
	public void set1Button(int ids1) {
		btn1.setImageResource(ids1);
		btnly1.setVisibility(View.VISIBLE);
	}

	// set Function Buttons
	public void set2Button(int ids1, int ids2) {
		btn1.setImageResource(ids1);
		btnly1.setVisibility(View.VISIBLE);
		btn2.setImageResource(ids2);
		btnly2.setVisibility(View.VISIBLE);
	}

	// set Function Buttons
	public void set3Buttons(int ids1, int ids2, int ids3) {
		btn1.setImageResource(ids1);
		btnly1.setVisibility(View.VISIBLE);
		btn2.setImageResource(ids2);
		btnly2.setVisibility(View.VISIBLE);
		btn3.setImageResource(ids3);
		btnly3.setVisibility(View.VISIBLE);
	}

	// set Function Buttons
	public void set4Buttons(int ids1, int ids2, int ids3, int ids4) {
		btn1.setImageResource(ids1);
		btnly1.setVisibility(View.VISIBLE);
		btn2.setImageResource(ids2);
		btnly2.setVisibility(View.VISIBLE);
		btn3.setImageResource(ids3);
		btnly3.setVisibility(View.VISIBLE);
		btn4.setImageResource(ids4);
		btnly4.setVisibility(View.VISIBLE);
	}

	// on click
	public interface OnActionBarBtnClick {
		public void onActionBarBtnClick(int id);
	}

	// on long click
	public interface OnActionBarBtnLongClick {
		public void onActionBarBtnLongClick(int id);
	}

	//
	private void setListener() {

		backLy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (clickListener != null) {
					clickListener.onActionBarBtnClick(BACK);
				}
			}
		});

		btnly1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (clickListener != null) {
					clickListener.onActionBarBtnClick(BTN1);
				}
			}
		});

		btnly2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (clickListener != null) {
					clickListener.onActionBarBtnClick(BTN2);
				}
			}
		});

		btnly3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (clickListener != null) {
					clickListener.onActionBarBtnClick(BTN3);
				}
			}
		});
		btnly4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (clickListener != null) {
					clickListener.onActionBarBtnClick(BTN4);
				}
			}
		});
	}
}
