package com.cl.partner.bean;

import com.jolo.fd.codec.bean.tlv.annotation.TLVAttribute;

/**
 * 游戏资源
 * 
 * @author jiangdehua
 * 
 */
public class ResGame {
	@TLVAttribute(tag = 10011105, charset = "UTF-8")
	public String gamePkgName;
	@TLVAttribute(tag = 10011109, charset = "UTF-8")
	public String gameName;
	@TLVAttribute(tag = 10011113, charset = "UTF-8")
	public String gameIconUrl;
	@TLVAttribute(tag = 10011112, charset = "UTF-8")
	public String gameDownloadUrl;
	@TLVAttribute(tag = 10011107, charset = "UTF-8")
	public String gameSizeNick;// 游戏大小 例如 1.10M
	@TLVAttribute(tag = 10011108)
	public Long gameSizeByte;// 游戏真实大小
	@TLVAttribute(tag = 10011110, charset = "UTF-8")
	public String gameDesc;// 游戏描述
	@TLVAttribute(tag = 10011101, charset = "UTF-8")
	public String gameVer;// 游戏版本号 1.1.3
	@TLVAttribute(tag = 10011134)
	public Integer gameVerInt;// 113000
	@TLVAttribute(tag = 10011158, charset = "UTF-8")
	public String gameClassName;// 游戏分类，多个用空格或者,分隔
	@TLVAttribute(tag = 10011155, charset = "UTF-8")
	public String gameDownloadCountNick;// 下载次数，给予什么就显示什么，例如：下载10w+,2013-05-10新增
	// 后来添加 游戏下载的状态和下载的百分比
	public int gameStatus; // 下载状态
	public int gamePrecent; // 下载百分比
	public long downloadID; //系统下载中对应的ID
}
