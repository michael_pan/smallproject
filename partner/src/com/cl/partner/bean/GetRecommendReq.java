package com.cl.partner.bean;

import com.jolo.fd.codec.bean.AbstractCommonBean;
import com.jolo.fd.codec.bean.UserAgent;
import com.jolo.fd.codec.bean.tlv.annotation.TLVAttribute;
/**
 * 获取交叉推荐资源
 * url:/getrecommend
 * @author jiangdehua
 *
 */
public class GetRecommendReq extends AbstractCommonBean{
	@TLVAttribute(tag=10020001)
	private UserAgent userAgent;

	public UserAgent getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(UserAgent userAgent) {
		this.userAgent = userAgent;
	}
}
