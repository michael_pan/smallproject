package com.cl.partner.bean;

import com.jolo.fd.codec.bean.AbstractCommonBean;
import com.jolo.fd.codec.bean.UserAgent;
import com.jolo.fd.codec.bean.tlv.annotation.TLVAttribute;


/**
 * 用户反馈
 * @author jiangdehua
 *
 */
public class SubmitFeedbackReq extends AbstractCommonBean{
	@TLVAttribute(tag=10020001)
	private UserAgent userAgent;
	
	@TLVAttribute(tag=10011146,charset="UTF-8")
	private String feedbackContent;//反馈内容
	
	@TLVAttribute(tag=10011024,charset="UTF-8")
	private String contact;//用户联系方式

	public UserAgent getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(UserAgent userAgent) {
		this.userAgent = userAgent;
	}

	public String getFeedbackContent() {
		return feedbackContent;
	}

	public void setFeedbackContent(String feedbackContent) {
		this.feedbackContent = feedbackContent;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}
}
