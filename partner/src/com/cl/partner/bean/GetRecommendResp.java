package com.cl.partner.bean;

import java.util.ArrayList;

import com.jolo.fd.codec.bean.BaseResp;
import com.jolo.fd.codec.bean.tlv.annotation.TLVAttribute;

/**
 * 获取交叉推荐游戏反馈
 * 
 * @author jiangdehua
 * 
 */
public class GetRecommendResp extends BaseResp {

	@TLVAttribute(tag = 10020004)
	private ArrayList<ResourceItem> itemList;

	public ArrayList<ResourceItem> getItemList() {
		return itemList;
	}

	public void setItemList(ArrayList<ResourceItem> itemList) {
		this.itemList = itemList;
	}
}
