package com.cl.partner.bean;

import com.jolo.fd.codec.bean.tlv.annotation.TLVAttribute;

/**
 * 交叉推荐资源
 * @author jiangdehua
 *
 */
public class ResourceItem {	
	@TLVAttribute(tag=10012005)
	private Byte itemType;//元素类型  1=游戏 2=wap类型
	@TLVAttribute(tag=10020003)
    private ResGame resGame;//如果itemType=1 那么这里不为null
	public Byte getItemType() {
		return itemType;
	}
	public void setItemType(Byte itemType) {
		this.itemType = itemType;
	}
	public ResGame getResGame() {
		return resGame;
	}
	public void setResGame(ResGame resGame) {
		this.resGame = resGame;
	}
}
