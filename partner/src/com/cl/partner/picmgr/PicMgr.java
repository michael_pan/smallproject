package com.cl.partner.picmgr;

import java.io.File;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.cl.partner.utils.FileUtils;

/**
 * 功能：定义统一的图片缓存入口，管理所有的图片缓存
 */
public class PicMgr {
	private static final String TAG = "PicMgr";
	// 内存Cache,用于 URL和Cache的对应Map
	private static HashMap<String, SoftReference<Drawable>> bitmapCache;
	// 用于存放ImageView和URL对应的Map
	private static Map<ImageView, String> imageViews;
	// 默认显示图片
	private int defImgID = 0;
	private static PicMgr sInstance = null;
	//
	private static boolean isDebug = false;

	public static PicMgr getInstance() {
		if (sInstance == null) {
			sInstance = new PicMgr();
		}
		return sInstance;
	}

	private PicMgr() {
		bitmapCache = new HashMap<String, SoftReference<Drawable>>();
		imageViews = Collections
				.synchronizedMap(new WeakHashMap<ImageView, String>());
	}

	/**
	 * 设置默认图片
	 * 
	 * @param id
	 */
	public void setDefaultImgId(int id) {
		defImgID = id;
	}

	// 加载图片,显示默认图片
	public void loadBitmap(ImageView iv, String url) {
		loadBitmap(iv, url, defImgID);
	}


	// 加载图片,可设置默认图片
	public void loadBitmap(final ImageView iv, final String url, final int defID) {
		Drawable drawable = null;
		if (TextUtils.isEmpty(url)) {
			if (isDebug) {
				Log.w(TAG, "url is empty!!!!");
			}
			setImage(iv, drawable, defID);
			return;
		}

		imageViews.put(iv, url);

		ImageCallback cb = new ImageCallback() {

			@Override
			public void onLoad(Drawable drawable, String url) {
				if (drawable == null)
					return;
				String oldurl = imageViews.get(iv);
				if (!TextUtils.isEmpty(oldurl) && oldurl.equals(url)) {
					setImage(iv, drawable, defID);
				}
			}
		};
		// 异步加载图片
		drawable = load(url, cb);
		// 设置图片
		setImage(iv, drawable, defID);
	}

	// 定义图片加载回调接口
	private interface ImageCallback {
		public void onLoad(Drawable drawable, String url);
	}

	// 加载的具体操作放这里
	private Drawable load(final String url, final ImageCallback callback) {
		if (url == null) {
			return null;
		}
		// 从缓存中取
		SoftReference<Drawable> ref = bitmapCache.get(url);

		Drawable drawable = (ref != null) ? ref.get() : null;
		if (drawable != null) {
			if (isDebug) {
				Log.w(TAG, "drawable is in cache" + " ,url = " + url);
			}
			return drawable;
		}
		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message message) {
				callback.onLoad((Drawable) message.obj, url);
			}
		};

		new Thread(new Runnable() {
			@Override
			public void run() {
				if (!imageViews.containsValue(url)) {
					if (isDebug) {
						Log.w(TAG, "url is outdate!!!!" + " ,url = " + url);
					}
					return;
				}
				// 从文件缓存读取
				Drawable drawable = getBitmapFromFile(url);
				if (drawable == null) {
					// 文件缓存没有，从网络获取
					drawable = getBitmapFromNet(url);
				}
				// if (isDebug && drawable!=null) {
				// JLog.e(TAG, "drawable W = "
				// + ((BitmapDrawable) drawable).getBitmap()
				// .getWidth()
				// + ",X H ="
				// + ((BitmapDrawable) drawable).getBitmap()
				// .getHeight());
				// }
				bitmapCache.put(url, new SoftReference<Drawable>(drawable));
				Message msg = Message.obtain();
				msg.obj = drawable;
				handler.sendMessage(msg);
			}
		}).start();

		return drawable;
	}

	// 设置Imageview的显示
	private void setImage(final ImageView iv, final Drawable drawable,
			final int defID) {
		// 如果是空，显示默认图片,设置背景色
		if (drawable == null && defID != 0) {
			iv.setImageResource(defID);
		} else if (drawable != null) {
			iv.setImageDrawable(drawable);
		}
	}

	// 从文件获取Bitmap
	public static Drawable getBitmapFromFile(String url) {
		final String imagePath = FileUtils.getPicPathFromURL(url);
		return getBitmapFromCachePath(imagePath);
	}

	// 从网络获取图片,并且保存到文件,做文件缓存
	public static Drawable getBitmapFromNet(String url) {
		final String imagePath = FileUtils.getPicPathFromURL(url);
		Bitmap bm = get(url);
		Drawable drawable = null;
		if(bm!=null){
			drawable = new BitmapDrawable(bm);
			FileUtils.Bitmap2PNG(bm, imagePath);
		}
		return drawable;
	}
	
	// 下载网络图片
	public static Bitmap get(String url) {
		if (url == null) {
			return null;
		}
		Bitmap bm = null;
		InputStream is = null;
		try {
			is = new DefaultHttpClient().execute(new HttpGet(url)).getEntity()
					.getContent();
			bm = BitmapFactory.decodeStream(is);
		} catch (Exception e) {

		} finally {
			try {
				is.close();
			} catch (Exception e) {
			}
		}
		return bm;
	}

	// 从文件中解码Bitmap
	private static Drawable getBitmapFromCachePath(String imagePath) {
		Drawable drawable = null;
		File file = new File(imagePath);
		if (file.exists() && file.isFile()) {
			drawable = Drawable.createFromPath(file.getPath());
			if (drawable == null) {
				file.delete();
			} else {
				//Log.e(TAG, "getBitmapFromCachePath success!!!");
				return drawable;
			}
		}
		return drawable;
	}

}