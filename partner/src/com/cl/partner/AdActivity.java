package com.cl.partner;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.cl.partner.adapter.GameListAdapter;
import com.cl.partner.bean.ResGame;
import com.cl.partner.database.GameStatus;
import com.cl.partner.download.DownloadCompleteReceiver;
import com.cl.partner.download.DownloadManagerPro;
import com.cl.partner.download.Partner;
import com.cl.partner.download.Partner.OnGetGamesSuccess;
import com.cl.partner.widget.ActionBar.ActionBar;
import com.cl.partner.widget.ActionBar.ActionBar.OnActionBarBtnClick;

public class AdActivity extends Activity implements OnActionBarBtnClick,
		OnGetGamesSuccess {
	ActionBar actionBar = null;
	private ListView lv = null;
	private GameListAdapter adapter = null;
	private DownloadManagerPro dm = null;
	private DownloadHandler handler = null;;
	private DownloadChangeObserver downloadObserver = null;
	private long downloadid = -1;
	private int pos = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ad_list);
		init();
		initview();
		Partner partner = Partner.getInstance(getApplicationContext());
		partner.getRecommend(this);
	}

	private void initview() {
		actionBar = (ActionBar) findViewById(R.id.actionBar);
		actionBar.setOnclickListener(this);
		actionBar.setTitle(R.string.ad_ads_name, R.drawable.icon);
		actionBar.showBackBtn(true);
		lv = (ListView) findViewById(R.id.ad_lv);
		adapter = new GameListAdapter(this, dm);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				downloadid = adapter.OnItemClick(position);
				pos = position;
			}
		});

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterDownloadReceiver();
	}

	@Override
	protected void onPause() {
		super.onPause();
		getContentResolver().unregisterContentObserver(downloadObserver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		getContentResolver().registerContentObserver(
				DownloadManagerPro.CONTENT_URI, true, downloadObserver);
		updateView();
	}

	// 注册手动下载完成的Receiver
	private DownloadCompleteReceiver downloadCompleteReceiver;

	private void registerDownloadReceiver() {
		downloadCompleteReceiver = new DownloadCompleteReceiver(dm);
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
		intentFilter.addAction(DownloadManager.ACTION_NOTIFICATION_CLICKED);
		registerReceiver(downloadCompleteReceiver, intentFilter);
	}

	private void unregisterDownloadReceiver() {
		if (downloadCompleteReceiver != null) {
			unregisterReceiver(downloadCompleteReceiver);
			downloadCompleteReceiver = null;
		}
	}

	private void init() {
		DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
		dm = new DownloadManagerPro(downloadManager);
		handler = new DownloadHandler();
		downloadObserver = new DownloadChangeObserver();
		registerDownloadReceiver();
	}

	private class DownloadChangeObserver extends ContentObserver {

		public DownloadChangeObserver() {
			super(handler);
		}

		@Override
		public void onChange(boolean selfChange) {
			updateView();
		}

	}

	public void updateView() {
		if (downloadid <= 0)
			return;
		Log.e("test", "downloadid = " + downloadid);
		int[] bytesAndStatus = dm.getBytesAndStatus(downloadid);
		handler.sendMessage(handler.obtainMessage(MSG_UPDATE_PRECENT,
				bytesAndStatus));
	}

	private static final int MSG_UPDATE_PRECENT = 1000;
	private static final int MSG_UPDATE_LIST = 1001;

	@SuppressLint("HandlerLeak")
	private class DownloadHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {

			switch (msg.what) {
			case MSG_UPDATE_PRECENT:
				int[] bytesAndStatus = (int[]) msg.obj;
				int sysstatus = bytesAndStatus[2];
				int status = GameStatus.NONE;
				if (sysstatus == DownloadManager.STATUS_PENDING)
					status = GameStatus.DOWNLOADING;
				else if (sysstatus == DownloadManager.STATUS_RUNNING)
					status = GameStatus.DOWNLOADING;
				else if (sysstatus == DownloadManager.STATUS_PAUSED)
					status = GameStatus.DOWNLOADING;
				else if (sysstatus == DownloadManager.STATUS_SUCCESSFUL)
					status = GameStatus.SUCCESS;
				else if (sysstatus == DownloadManager.STATUS_FAILED)
					status = GameStatus.SUCCESS;
				int precent = (int) ((bytesAndStatus[0] * 100.0f) / (1.0f * bytesAndStatus[1]));
				adapter.updateView(downloadid, pos, status, precent);
				Log.e("test", "precent = " + precent);
				break;
			case MSG_UPDATE_LIST:
				ArrayList<ResGame> games = (ArrayList<ResGame>) (msg.obj);
				adapter.updateGames(games);
				break;
			}
		}
	}

	@Override
	public void onActionBarBtnClick(int id) {
		switch (id) {
		case ActionBar.BACK:
			finish();
			Log.e("adactivity", "ActionBar.BACK....");
			break;
		case ActionBar.BTN1:
			Log.e("adactivity", "ActionBar.BTN1....");
			break;
		case ActionBar.BTN2:
			gotoFeedback();
			Log.e("adactivity", "ActionBar.BTN2....");
			break;
		case ActionBar.BTN3:
			Log.e("adactivity", "ActionBar.BTN3....");
			break;
		case ActionBar.BTN4:
			Log.e("adactivity", "ActionBar.BTN4....");
			break;
		default:
			break;
		}
	}

	private void gotoFeedback() {
		Intent intent = new Intent(this, FeedbackActivity.class);
		startActivity(intent);
	}

	@Override
	public void onGetGamesSuccess(ArrayList<ResGame> games) {
		handler.sendMessage(handler.obtainMessage(MSG_UPDATE_LIST, games));
	}

}
