package com.cl.partner.download;

import java.io.File;

import android.os.Environment;

public class GLOBAL {
	
	public static String SERVER_URL = "http://115.29.147.92/";
	
	// 定义apk下载目录和图片下载目录
	private static final String DOWNLOADPATH = Environment
			.getExternalStorageDirectory().getAbsolutePath()
			+ File.separator
			+ ".partner";
	// APK下载地址
	public static final String APKDOWNLOAD = DOWNLOADPATH + File.separator
			+ "download" + File.separator + "apk";
	// 图片下载地址
	public static final String PICDOWNLOAD = DOWNLOADPATH + File.separator
			+ "download" + File.separator + "pic";
}
