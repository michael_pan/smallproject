package com.cl.partner.download;

public class DownloadStatus {
	//无下载
	public static final int NONE = 1000;
	//下载中
	public static final int DOING = NONE + 1;
	//等待中
	public static final int WAITING = NONE + 2;
	//下载成功
	public static final int SUCCESS = NONE + 3;
	//下载失败
	public static final int FAIL = NONE + 4;
}
