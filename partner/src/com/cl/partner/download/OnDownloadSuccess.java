package com.cl.partner.download;

public interface OnDownloadSuccess {
	public void onDownloadSuccess(long pushID, boolean isSuccess);
}
