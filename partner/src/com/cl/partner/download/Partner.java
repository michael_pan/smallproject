package com.cl.partner.download;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;

import com.cl.partner.bean.ResGame;
import com.cl.partner.bean.ResourceItem;
import com.cl.partner.database.ADSDBHelper;
import com.cl.partner.database.DLDBHelper;
import com.cl.partner.httpconnect.HttpConnect;
import com.cl.partner.utils.ClientInfo;
import com.cl.partner.utils.CommonPreferences;
import com.jolo.fd.codec.bean.UserAgent;

public class Partner {
	private static final String TAG = "Downloader";
	private static Context ctx;
	private static ExecutorService singlePool;
	private static UserAgent userAgent;
	private static Partner partner;
	private static ADSDBHelper adsHelper;
	private static DLDBHelper dlHelper;

	private Partner() {

	}

	public static Partner getInstance(Context context) {
		if (partner == null) {
			synchronized (Partner.class) {
				if (partner == null) {
					partner = new Partner();
					init(context);
				}
			}
		}
		return partner;
	}

	// 获取推荐列表
	public void getRecommend(final OnGetGamesSuccess listener) {
		singlePool.execute(new Runnable() {

			@Override
			public void run() {
				long lasttime = CommonPreferences.getADSUpdateTime(ctx);
				ArrayList<ResGame> games = null;
				if (System.currentTimeMillis() - lasttime > DateUtils.DAY_IN_MILLIS) {
					ArrayList<ResourceItem> items = HttpConnect
							.getRecommend(userAgent);
					games = getGames(items);
					if (games != null && games.size() > 0) {
						adsHelper.insertAll(games);
					}
					CommonPreferences.saveADSUpdateTime(ctx,
							System.currentTimeMillis());
				} else {
					games = adsHelper.queryAll();
				}
				if (listener != null)
					listener.onGetGamesSuccess(games);
			}
		});
	}

	// 发送反馈
	public void feedback(final String contact, final String content) {
		singlePool.execute(new Runnable() {

			@Override
			public void run() {
				boolean isSuccess = HttpConnect.feedback(userAgent, contact,
						content);
				Log.i(TAG, "feedback success = " + isSuccess);
			}
		});
	}

	private static void init(Context context) {
		ctx = context;
		singlePool = Executors
				.newSingleThreadExecutor(new PriorityThreadFactory("partner"));
		userAgent = getUserAgent();
		adsHelper = new ADSDBHelper(context);
		adsHelper.open();
		dlHelper = new DLDBHelper(context);
		dlHelper.open();
	}

	private static UserAgent getUserAgent() {
		ClientInfo clientInfo = ClientInfo.getInstance(ctx);
		UserAgent ua = new UserAgent();
		ua.setAndroidSystemVer(clientInfo.getAndroidVer());
		ua.setApkVer(clientInfo.getApkVer());
		ua.setCpu(clientInfo.getCpu());
		ua.setHsman(clientInfo.getHsman());
		ua.setHstype(clientInfo.getHstype());
		ua.setImei(clientInfo.getImei());
		ua.setImsi(clientInfo.getImsi());
		ua.setNetworkType(clientInfo.getNetworkType());
		ua.setPackegeName(clientInfo.getPackageName());
		ua.setProvider(clientInfo.getProvider());
		ua.setRamSize(clientInfo.getRamSize());
		ua.setRomSize(clientInfo.getRomSize());
		ua.setScreenSize(clientInfo.getScreenSize());
		ua.setDpi(clientInfo.getDpi());
		ua.setMac(clientInfo.getMac());
		return ua;
	}

	private ArrayList<ResGame> getGames(ArrayList<ResourceItem> items) {
		ArrayList<ResGame> games = new ArrayList<ResGame>();
		if (items == null || items.size() < 1)
			return games;
		for (ResourceItem item : items) {
			if (item.getItemType() == 1)
				games.add(item.getResGame());
		}
		return games;
	}

	public interface OnGetGamesSuccess {
		public void onGetGamesSuccess(ArrayList<ResGame> games);
	}
}
