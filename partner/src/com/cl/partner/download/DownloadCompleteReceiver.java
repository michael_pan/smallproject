package com.cl.partner.download;

import com.cl.partner.utils.UIUtils;

import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * 系统下载完成时的广播通知
 * 
 * @author PanYingYun
 * 
 */
public class DownloadCompleteReceiver extends BroadcastReceiver {

	private final String TAG = "DownloadCompleteReceiver";
	private DownloadManagerPro dm = null;

	public DownloadCompleteReceiver(DownloadManagerPro mgr) {
		dm = mgr;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent == null || intent.getAction() == null)
			return;
		String action = intent.getAction();
		if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
			long downloadid = intent.getLongExtra(
					DownloadManager.EXTRA_DOWNLOAD_ID, -1);
			Log.e(TAG, "DownloadManager ACTION_DOWNLOAD_COMPLETE");
			Log.e(TAG, "downloadid = " + downloadid);
			if (dm != null && downloadid > 0) {
				String localuri = dm.getLocalUri(downloadid);
				Log.e(TAG, "localuri = " + localuri);
				UIUtils.gotoIntalllUri(context, localuri);
			}
		} else if (action.equals(DownloadManager.ACTION_NOTIFICATION_CLICKED)) {
			startViewDownloaders(context);
		}
	}

	private void startViewDownloaders(Context ctx) {
		Intent intent = new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try {
			Log.e("error", "start view downloads");
			ctx.startActivity(intent);
		} catch (Exception e) {
			Log.e("error", "not found action");
		}
	}

}
