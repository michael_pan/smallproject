package com.cl.partner;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cl.partner.download.Partner;
import com.cl.partner.widget.ActionBar.ActionBar;
import com.cl.partner.widget.ActionBar.ActionBar.OnActionBarBtnClick;

public class FeedbackActivity extends Activity implements OnActionBarBtnClick,
		OnClickListener {

	private ActionBar actionBar;
	private Button commitBtn;
	private EditText content;
	private EditText contact;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ad_feedback);
		init();
		initview();
	}

	private void init() {

	}

	private void initview() {
		actionBar = (ActionBar) findViewById(R.id.actionBar);
		actionBar.setOnclickListener(this);
		actionBar.setTitle(R.string.ad_feedback, R.drawable.icon);
		actionBar.showBackBtn(true);
		commitBtn = (Button) findViewById(R.id.fb_commit_btn);
		commitBtn.setOnClickListener(this);
		content = (EditText) findViewById(R.id.fb_content);
		contact = (EditText) findViewById(R.id.fb_contact);
	}

	@Override
	public void onActionBarBtnClick(int id) {
		switch (id) {
		case ActionBar.BACK:
			finish();
			break;
		case ActionBar.BTN1:
			Log.e("FeedbackActivity", "ActionBar.BTN1....");
			break;
		case ActionBar.BTN2:
			Log.e("FeedbackActivity", "ActionBar.BTN2....");
			break;
		case ActionBar.BTN3:
			Log.e("FeedbackActivity", "ActionBar.BTN3....");
			break;
		case ActionBar.BTN4:
			Log.e("FeedbackActivity", "ActionBar.BTN4....");
			break;
		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		String strContent = content.getText().toString();
		String strContact = contact.getText().toString();
		if (TextUtils.isEmpty(strContact))
			strContact = "";
		if (TextUtils.isEmpty(strContent)) {
			Toast.makeText(this, R.string.ad_warning, Toast.LENGTH_SHORT)
					.show();
		} else {
			Partner partner = Partner.getInstance(getApplicationContext());
			partner.feedback(strContact, strContent);
			Log.e("FeedbackActivity", "strContact = " + strContact);
			Log.e("FeedbackActivity", "strContent = " + strContent);
			Toast.makeText(this, R.string.ad_thanks, Toast.LENGTH_SHORT).show();
			finish();
		}
	}
}
