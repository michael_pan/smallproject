package com.cl.partner;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.cl.partner.download.Partner;
import com.cl.partner.utils.CommonPreferences;
import com.cl.partner.utils.UIUtils;
import com.cl.partner.widget.ActionBar.ActionBar;
import com.cl.partner.widget.ActionBar.ActionBar.OnActionBarBtnClick;

public class TestActivity extends Activity implements OnActionBarBtnClick {

	private static final String TAG = "TestActivity";
	private ActionBar actionBar = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ad_list);
		initview();
		// ��ȡ��Ϸ
		Partner partner = Partner.getInstance(getApplicationContext());
		partner.getRecommend(null);
	}

	private void initview() {
		actionBar = (ActionBar) findViewById(R.id.actionBar);
		actionBar.setOnclickListener(this);
		actionBar.setTitle(R.string.ad_app_name, R.drawable.icon);
		actionBar.showBackBtn(false);
		long time = CommonPreferences.getADSUpdateTime(this);
		if (System.currentTimeMillis() - time > 24 * 3600) {
			actionBar.set2Button(R.drawable.ad_actionbar_game_red,
					R.drawable.ad_actionbar_feedback);
		} else {
			actionBar.set2Button(R.drawable.ad_actionbar_game,
					R.drawable.ad_actionbar_feedback);
		}
	}

	@Override
	public void onActionBarBtnClick(int id) {
		switch (id) {
		case ActionBar.BACK:
			Log.e(TAG, "ActionBar.BACK....");
			break;
		case ActionBar.BTN1:
			UIUtils.gotoGames(this);
			Log.e(TAG, "ActionBar.BTN1....");
			break;
		case ActionBar.BTN2:
			UIUtils.gotoFeedback(this);
			Log.e(TAG, "ActionBar.BTN2....");
			break;
		case ActionBar.BTN3:
			Log.e(TAG, "ActionBar.BTN3....");
			break;
		case ActionBar.BTN4:
			Log.e(TAG, "ActionBar.BTN4....");
			break;
		default:
			break;
		}
	}

}
