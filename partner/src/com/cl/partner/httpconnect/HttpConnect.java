package com.cl.partner.httpconnect;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;

import android.os.SystemClock;
import android.util.Log;

import com.cl.partner.bean.GetRecommendReq;
import com.cl.partner.bean.GetRecommendResp;
import com.cl.partner.bean.ResourceItem;
import com.cl.partner.bean.SubmitFeedbackReq;
import com.cl.partner.bean.SubmitFeedbackResp;
import com.cl.partner.download.GLOBAL;
import com.jolo.fd.codec.bean.UserAgent;
import com.jolo.fd.codec.bean.tlv.decode.decoders.BeanTLVDecoder;
import com.jolo.fd.codec.bean.tlv.encode.TLVEncodeContext;
import com.jolo.fd.codec.bean.tlv.encode.encoders.BeanTLVEncoder;
import com.jolo.fd.util.ByteUtils;

/**
 * 
 * @author PanYingYun
 * 
 */
public class HttpConnect {
	private static String TAG = "HttpConnect";

	private static int RETRY_TIME = 3;
	private final static int TIMEOUT_CONNECTION = 20000;
	private final static int TIMEOUT_SOCKET = 15000;

	private static HttpClient getHttpClient() {
		HttpClient httpClient = new DefaultHttpClient();
		httpClient.getParams().setParameter(
				CoreConnectionPNames.CONNECTION_TIMEOUT, TIMEOUT_CONNECTION);
		httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,
				TIMEOUT_SOCKET);
		return httpClient;
	}

	private static Object postMethod(Object request, String uriString,
			Class<?> resptype) {
		//Log.e(TAG, "http uriString :" + uriString);
		Object resp = null;
		/* HTTP Post */
		HttpPost httpRequest = new HttpPost(uriString);
		httpRequest.setHeader("content-type", "application/x-tar");

		BeanTLVEncoder beanEncoder = new BeanTLVEncoder();
		TLVEncodeContext encode = beanEncoder.getEncodeContextFactory()
				.createEncodeContext(request.getClass(), null);
		byte[] data = ByteUtils.union(beanEncoder.encode(request, encode));
		//Log.e(TAG, "body byte length:" + data.length);
		//Log.e(TAG, "body byte:" + data);

		httpRequest.setEntity(new InputStreamEntity(new ByteArrayInputStream(
				data), data.length));

		HttpClient httpClient = getHttpClient();
		//Log.i(TAG, "Request is initiated,is requesting...");
		//
		HttpResponse httpResponse = null;
		int status = -1;
		int time = 0;
		do {
			try {
				httpResponse = httpClient.execute(httpRequest);
				status = httpResponse.getStatusLine().getStatusCode();
				break;
			} catch (Exception e) {
				time++;
				if (time < RETRY_TIME) {
					SystemClock.sleep(1000);
					continue;
				}
				Log.e(TAG, "get response status error time = " + time);
			}
		} while (time < RETRY_TIME);

		if (status == 200) {
			//
			//Log.e(TAG, "Is to decode the entity ...");
			// InputStream is = null;
			try {
				byte[] body = EntityUtils.toByteArray(httpResponse.getEntity());
				//Log.e(TAG,"body = "+ ByteUtils.bytesAsHexString(body,
				//1000));
				//Log.e(TAG, "body length = " + body.length);
				BeanTLVDecoder beanDecoder = new BeanTLVDecoder();
				resp = beanDecoder.decode(body.length, body,
						beanDecoder.getDecodeContextFactory()
								.createDecodeContext(resptype, null));
			} catch (Exception e) {
				Log.e(TAG, "decode error = " + e);
			}
		} else {
			Log.e(TAG, "Returns status is: " + status);
		}

		if (httpClient != null) {
			httpClient.getConnectionManager().shutdown();
			httpClient = null;
		}
		return resp;
	}

	// 获取推荐列表
	public static ArrayList<ResourceItem> getRecommend(UserAgent ua) {
		GetRecommendReq req = new GetRecommendReq();
		req.setUserAgent(ua);
		GetRecommendResp resp = (GetRecommendResp) HttpConnect.postMethod(req,
				GLOBAL.SERVER_URL + "getrecommend", GetRecommendResp.class);
		if (resp != null && resp.getResponseCode() == 200) {
			return resp.getItemList();
		} else {
			return null;
		}
	}

	// 用户反馈
	public static boolean feedback(UserAgent ua,String contact, String content ) {
		boolean isSuccess = false;
		SubmitFeedbackReq req = new SubmitFeedbackReq();
		req.setUserAgent(ua);
		req.setContact(contact);
		req.setFeedbackContent(content);
		SubmitFeedbackResp resp = (SubmitFeedbackResp) HttpConnect.postMethod(
				req, GLOBAL.SERVER_URL + "submitfeedback",
				SubmitFeedbackResp.class);
		if (resp != null && resp.getResponseCode() == 200) {
			isSuccess = true;
		}
		return isSuccess;
	}
}