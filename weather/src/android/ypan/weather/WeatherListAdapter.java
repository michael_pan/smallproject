package android.ypan.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.ypan.weather.bean.AllEntry;
import android.ypan.weather.bean.SixDayEntry;
import android.ypan.weather.bean.TodayEntry;
import android.ypan.weather.bean.TodayNowEntry;
import android.ypan.weather.utils.AndroidUtils;
import android.ypan.weather.widget.FutureAdapter;

public class WeatherListAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private Context mContext;

	public WeatherListAdapter(Context ctx, LayoutInflater inflater) {
		mContext = ctx;
		mInflater = inflater;
	}

	@Override
	public int getCount() {
		return MainApp.citys.size();
	}

	@Override
	public Object getItem(int position) {
		return MainApp.getEntry(MainApp.citys.get(position));
	}

	// 更新天气
	public void updateWeather(AllEntry entry) {
		notifyDataSetChanged();
	}

	// 更新时间
	public void updateTime() {
		notifyDataSetChanged();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.weather_list_item, null);
			holder = new ViewHolder();
			holder.cityTV = (TextView) convertView.findViewById(R.id.item_city);
			holder.weatherTV = (TextView) convertView
					.findViewById(R.id.item_weather);
			holder.windTV = (TextView) convertView.findViewById(R.id.item_wind);
			holder.curTempTV = (TextView) convertView
					.findViewById(R.id.item_cur_temp);
			holder.highTempTV = (TextView) convertView
					.findViewById(R.id.item_high_temp);
			holder.lowTempTV = (TextView) convertView
					.findViewById(R.id.item_low_temp);
			holder.updatetimeTV = (TextView) convertView
					.findViewById(R.id.item_updatetime);
			holder.curtimeTV = (TextView) convertView
					.findViewById(R.id.item_currenttime);
			holder.featureGv = (GridView) convertView
					.findViewById(R.id.weather_feature);
			holder.fadapter = new FutureAdapter(mContext, mInflater);
			holder.featureGv.setAdapter(holder.fadapter);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		String city = MainApp.citys.get(position);
		if (city == null || MainApp.weathers.get(city) == null)
			return convertView;
		TodayNowEntry nowEntry = MainApp.weathers.get(city).getTodayNow();
		TodayEntry entry = MainApp.weathers.get(city).getToday();
		holder.cityTV.setText(nowEntry.getCity());
		holder.weatherTV.setText(entry.getWeather());
		String sd = mContext.getResources().getString(R.string.item_sd,
				nowEntry.getSd());
		holder.windTV.setText(nowEntry.getWd() + " " + nowEntry.getWs() + sd);
		holder.curTempTV.setText(nowEntry.getTemp() + "℃");
		String highTemp = entry.getHighTemp();
		String lowTemp = entry.getLowTemp();
		holder.highTempTV.setText(highTemp);
		holder.lowTempTV.setText(lowTemp);

		String date = AndroidUtils.getCurDate2() + " " + AndroidUtils.getCurWeek()
				+ " " + AndroidUtils.getChinaDate();
		holder.curtimeTV.setText(date);

		holder.updatetimeTV.setText(mContext.getResources().getString(
				R.string.item_updatetime)
				+ nowEntry.getTime());

		SixDayEntry sixEntry = MainApp.weathers.get(city).getFuture();
		holder.fadapter.setFuture(sixEntry);
		return convertView;
	}

	private static class ViewHolder {
		TextView cityTV;
		TextView weatherTV;
		TextView windTV;
		TextView curTempTV;
		TextView highTempTV;
		TextView lowTempTV;
		TextView updatetimeTV;
		TextView curtimeTV;
		GridView featureGv;
		FutureAdapter fadapter;
	}

}
