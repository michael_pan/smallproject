package android.ypan.weather;

public class NotifyMsg {
	public static final int MSG_BASE = 0x1000;
	public static final int MSG_BINDOK = MSG_BASE + 1;
	public static final int MSG_ALL = MSG_BASE + 2;
	public static final int MSG_CLOCK = MSG_BASE + 3;
	public static final int MSG_LBS = MSG_BASE + 4;
}
