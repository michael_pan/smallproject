package android.ypan.weather.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import android.ypan.weather.R;


public class CustomWaitingDialog extends Dialog{

	private int mMsgID = R.string.dlg_lbs_start;
	public CustomWaitingDialog(Context context, int msgID) {
		super(context,R.style.WaitingDialogStyle);
		mMsgID = msgID;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.waiting_dialog);
		TextView tv = (TextView)findViewById(R.id.txt_custom_content);
		tv.setText(mMsgID);
		setCancelable(false);
	}
	
	
}
