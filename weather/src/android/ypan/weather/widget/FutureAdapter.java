package android.ypan.weather.widget;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.ypan.weather.R;
import android.ypan.weather.bean.SixDayEntry;
import android.ypan.weather.utils.AndroidUtils;

public class FutureAdapter extends BaseAdapter {

	private long oneday = 24 * 3600 * 1000;
	private LayoutInflater mInflater;
	private Context mContext;
	// δ������
	private SixDayEntry mSixDayEntry;

	public FutureAdapter(Context ctx, LayoutInflater inflater) {
		mContext = ctx;
		mInflater = inflater;
	}

	public void setFuture(SixDayEntry entry) {
		mSixDayEntry = entry;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return (mSixDayEntry != null) ? mSixDayEntry.getTempList().size() : 0;
	}

	@Override
	public Object getItem(int pos) {
		return pos;
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater
					.inflate(R.layout.weather_feature_item, null);
			holder = new ViewHolder();
			holder.dateTV = (TextView) convertView
					.findViewById(R.id.feature_date);
			holder.tempTV = (TextView) convertView
					.findViewById(R.id.feature_temp);
			holder.weatherTV = (TextView) convertView
					.findViewById(R.id.feature_weather);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		ArrayList<String> tempList = mSixDayEntry.getTempList();
		ArrayList<String> weatherList = mSixDayEntry.getWeatherList();
		long week = System.currentTimeMillis() + position * oneday;
		if (position == 0) {
			holder.dateTV.setText(R.string.item_today);
		} else {
			holder.dateTV.setText(AndroidUtils.getWeek(week));
		}
		holder.tempTV.setText(tempList.get(position));
		holder.weatherTV.setText(weatherList.get(position));
		return convertView;
	}

	private static class ViewHolder {
		TextView dateTV;
		TextView tempTV;
		TextView weatherTV;
	}

}
