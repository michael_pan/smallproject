package android.ypan.weather;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.ypan.weather.bean.AllEntry;
import android.ypan.weather.utils.BitmapManager;
import android.ypan.weather.utils.CommonPreferences;
import android.ypan.weather.utils.SLog;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

public class MainApp extends Application {

	// 城市管理
	private static CityManager cityMgr = null;
	// 定位管理
	private static LocationClient lbsClient = null;
	// 图片缓冲管理
	private static BitmapManager bitmapMgr = null;
	// 城市列表
	public static ArrayList<String> citys = null;
	// 天气列表
	public static HashMap<String, AllEntry> weathers = new HashMap<String, AllEntry>();
	//
	private static Context ctx;
	//
	public static Activity sMainActivity;
	
	@Override
	public void onCreate() {
		super.onCreate();
		ctx = this;
		// 初始化日志
		SLog.init(true, false, SLog.LEVEL_DEBUG, false);
		// 初始化城市列表
		cityMgr = CityManager.getCityManager(this);
		// 创建定位器
		initBaiduLbsClient();
		// 图片缓存
		bitmapMgr = new BitmapManager(BitmapFactory.decodeResource(
				getResources(), R.drawable.weather_default_icon));
		// 初始化城市
		initCitys();
		// 启动天气服务
		startNotifyService();

	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

	//获取单列句柄
	public static MainApp getInstance(){
		return (MainApp)ctx;
	}
	// 城市管理
	public static CityManager getCityManager() {
		return cityMgr;
	}

	// 定位
	public static LocationClient getLBSClient() {
		return lbsClient;
	}

	// 图片缓冲管理
	public static BitmapManager getBitmapMgr() {
		return bitmapMgr;
	}

	// 获取天气
	public static AllEntry getEntry(String city) {
		if (TextUtils.isEmpty(city))
			return null;
		return weathers.get(city);
	}

	// 设置天气
	public static void setEntry(String city, AllEntry entry) {
		if (TextUtils.isEmpty(city) || entry == null)
			return;
		weathers.put(city, entry);
	}

	// 初始化城市
	private void initCitys() {
		citys = CommonPreferences.getCitys(ctx);
//		if(citys.size() == 0){
//			citys.add("杭州");
//			citys.add("兴化");
//			citys.add("阜宁");
//			citys.add("昆山");
//		}
	}

	// 添加城市
	public static ArrayList<String> addCity(String city) {
		if (!citys.contains(city)) {
			citys.add(city);
			CommonPreferences.saveCitys(ctx, citys);
		}
		return citys;
	}

	// 启动服务
	private void startNotifyService() {
		startService(new Intent(ctx, NotifyService.class));
	}

	// Baidu loc
	private void initBaiduLbsClient() {
		lbsClient = new LocationClient(this);
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(false); // 打开gps
		// option.setCoorType(mCoorEdit.getText().toString()); //设置坐标类型
		option.setServiceName("com.baidu.location.service_v2.9");
		option.setPoiExtraInfo(false);
		option.setAddrType("all");
		option.setScanSpan(0);
		option.setPriority(LocationClientOption.NetWorkFirst); // 设置网络优先
		option.setPoiNumber(10);
		option.disableCache(true);
		lbsClient.setLocOption(option);
	}

	public void getLBSCity(Handler cb) {
		LocListenner listenner = new LocListenner(cb);
		lbsClient.registerLocationListener(listenner);
		lbsClient.start();
		lbsClient.requestLocation();
	}

	/**
	 * 监听函数，又新位置的时候，格式化成字符串，输出到屏幕中
	 */
	public class LocListenner implements BDLocationListener {
		private Handler mHandler=null;

		public LocListenner(Handler handler) {
			mHandler = handler;
		}

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null)
				return;
			//去掉“市”
			String locCity = location.getCity().replaceAll("市", "");
			if (mHandler != null) {
				Message msg = new Message();
				msg.what = NotifyMsg.MSG_LBS;
				msg.obj = locCity;
				mHandler.sendMessage(msg);
			}
			SLog.i("LBS", "baidu city = " + locCity);
		}

		@Override
		public void onReceivePoi(BDLocation location) {
		}
	}

}
