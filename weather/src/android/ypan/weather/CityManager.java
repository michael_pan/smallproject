package android.ypan.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.Context;
import android.ypan.weather.bean.CityEntry;
import android.ypan.weather.utils.SLog;

/**
 * 城市列表
 * @author Administrator
 *
 */
public class CityManager {
	private static final String TAG = CityManager.class.getSimpleName();
	private static CityManager instance;
	private static ArrayList<CityEntry> mCitys = new ArrayList<CityEntry>();
	private CityManager() {
	}

	/**
	 * 单一实例,城市列表
	 */
	public static CityManager getCityManager(Context context) {
		if (instance == null) {
			instance = new CityManager();
			initCitys(context);
		}
		return instance;
	}

	private static void initCitys(Context context) {
		InputStream is = context.getResources()
				.openRawResource(R.raw.city_code);
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line;
		String[] city;
		try {
			while ((line = reader.readLine()) != null) {
				city = line.split(" ");
				mCitys.add(new CityEntry(city[0], city[1], city[2]));
			}
			reader.close();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
			SLog.e(TAG, "read error");
		}
		SLog.d(TAG, "mCitys size = "+mCitys.size());
	}

	//通过城市名字获取城市ID
	public String getCityIDByName(String name){
		if(isEmpty(name))
			return null;
		CityEntry findEntry = null;
		for(CityEntry entry: mCitys){
			if(name.trim().equals(entry.getName())){
				findEntry = entry;
				break;
			}
		}
		return findEntry!=null? findEntry.getID(): null;
	}
	
	//通过城市拼音获取城市ID
	public String getCityIDByPinYin(String pinyin){
		if(isEmpty(pinyin))
			return null;
		CityEntry findEntry = null;
		for(CityEntry entry: mCitys){
			if(pinyin.trim().equals(entry.getPinyin())){
				findEntry = entry;
				break;
			}
		}
		return findEntry!=null? findEntry.getID(): null;
		
	}
	
	private boolean isEmpty(String str){
		if(str==null || str.trim().length() == 0){
			return true;
		}else {
			return false;
		}
	}
}
