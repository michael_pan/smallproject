package android.ypan.weather;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.ypan.weather.utils.SLog;

public class NetworkChangedReceiver extends BroadcastReceiver{
    public static final String ACTION_NET = "android.net.conn.CONNECTIVITY_CHANGE";
    public static final String TAG = "NetworkChangedReceiver";
	@Override
	public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_NET)) {
            // start weather service
            Intent intentService = new Intent(context, NotifyService.class);
            context.startService(intentService);
            SLog.i(TAG, "NetworkChangedReceiver  start notify service");
        }
	}

}
