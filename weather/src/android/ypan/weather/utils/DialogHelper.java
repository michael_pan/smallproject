package android.ypan.weather.utils;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.ypan.weather.MainApp;
import android.ypan.weather.R;
import android.ypan.weather.widget.CustomWaitingDialog;

public class DialogHelper {

	public static void Toast(int resId, boolean isLong) {
		int duration = isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(MainApp.sMainActivity, resId, duration);
		toast.setGravity(Gravity.BOTTOM, 0, 50);
		toast.show();
	}

	public static AlertDialog showAboutDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				MainApp.sMainActivity);
		builder.setTitle(R.string.title_about);
		View view = LayoutInflater.from(MainApp.sMainActivity).inflate(
				R.layout.dialog_about, null);
		TextView version = (TextView) view.findViewById(R.id.version);
		version.append(" ");
		version.append(MainApp.sMainActivity.getResources().getString(
				R.string.app_name));
		version.append(" ");
		version.append(getVersion());
		TextView email =(TextView)view.findViewById(R.id.email);
		email.append("panyingyun"+"@"+"gmail"+".com");
		builder.setView(view);
		builder.setNeutralButton(MainApp.sMainActivity.getResources()
				.getString(R.string.ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				return;
			}
		});
		return builder.create();
	}

	private static String getVersion() {
		try {
			return MainApp.sMainActivity.getPackageManager().getPackageInfo(
					MainApp.sMainActivity.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			return "version not found";
		}
	}
	
	public static Dialog createWaitingDialog(Context ctx, int strID){
		return new CustomWaitingDialog(ctx,strID);
	}

}
