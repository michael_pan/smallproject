package android.ypan.weather.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class ToastUtils {
	/**
	 * UI线程可调用
	 * 
	 * @param str
	 */
	public static void showUIToast(Context ctx, String str) {
		Toast toast = Toast.makeText(ctx, str, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.show();
	}

	/**
	 * UI线程可调用
	 * 
	 * @param strID
	 */
	public static void showUIToast(Context ctx, int strID) {
		Toast toast = Toast.makeText(ctx, strID, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.show();
	}
}
