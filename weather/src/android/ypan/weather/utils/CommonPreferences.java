package android.ypan.weather.utils;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class CommonPreferences {
	//save citys
	private static final String CityHistory = "CityHistory";
	private static final String CityHead = "city_";
	//save today weather
	private static final String TodayHistory = "TodayHistory";
	private static final String TodayHead = "today_";
	//save feather weather
	private static final String FutureHistory = "FutureHistory";
	private static final String FutureHead = "future_";

	public static void saveCitys(Context ctx, ArrayList<String> citys){
		saveList(ctx,citys,CityHistory,CityHead);
	}
	
	public static ArrayList<String> getCitys(Context ctx){
		return getList(ctx, CityHistory, CityHead);
	}
	
	public static void saveTodays(Context ctx, ArrayList<String> todays){
		saveList(ctx, todays, TodayHistory, TodayHead);
	}
	
	public static ArrayList<String> getTodays(Context ctx){
		return getList(ctx, TodayHistory, TodayHead);
	}
	
	public static void saveFutuers(Context ctx, ArrayList<String> futures){
		saveList(ctx, futures, FutureHistory, FutureHead);
	}
	
	public static ArrayList<String> getFutures(Context ctx){
		return getList(ctx, FutureHistory, FutureHead);
	}
	
	private static void saveList(Context ctx , ArrayList<String> list, String fileName,String head){
		if(ctx==null || list.size()==0){
			return;
		}
		clear(ctx, fileName);
		saveData(ctx, fileName, head+"size", list.size());
		for(int i=0; i<list.size(); i++){
			saveData(ctx, fileName, head+i, list.get(i));
		}
	}
	
	private static ArrayList<String> getList(Context ctx, String fileName, String head){
		ArrayList<String> list = new ArrayList<String>();
		int size = (Integer)getData(ctx, fileName, head+"size", 0);
		for(int i=0; i<size; i++){
			list.add((String)getData(ctx, fileName, head+i, ""));
		}
		return list;
	}
	
	/**
	 * 保存数据
	 * 
	 * @param context
	 * @param fileName
	 * @param key
	 * @param value
	 */
	private static void saveData(Context ctx, String fileName, String key, Object value) {
		SharedPreferences sp = 
				ctx.getSharedPreferences(fileName, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		if (value instanceof Boolean) {
			editor.putBoolean(key, (Boolean) value);
		} else if (value instanceof Integer) {
			editor.putInt(key, (Integer) value);
		} else if (value instanceof Long) {
			editor.putLong(key, (Long) value);
		} else if (value instanceof Float) {
			editor.putFloat(key, (Float) value);
		} else {
			if (value == null) {
				editor.putString(key, "");
			} else {
				editor.putString(key, String.valueOf(value));
			}
		}
		editor.commit();
	}

	/**
	 * 取值
	 * 
	 * @param context
	 * @param fileName
	 * @param key
	 * @param defValue
	 * @return
	 */
	private static Object getData(Context ctx,String fileName, String key, Object defValue) {
		SharedPreferences sp = ctx.getSharedPreferences(fileName, Context.MODE_PRIVATE);
		if (defValue instanceof Boolean) {
			return sp.getBoolean(key, (Boolean) defValue);
		} else if (defValue instanceof Integer) {
			return sp.getInt(key, (Integer) defValue);
		} else if (defValue instanceof Long) {
			return sp.getLong(key, (Long) defValue);
		} else if (defValue instanceof Float) {
			return sp.getFloat(key, (Float) defValue);
		} else {
			if (defValue == null) {
				return sp.getString(key, "");
			}
			return sp.getString(key, String.valueOf(defValue));
		}
	}

	/**
	 * 清空
	 * 
	 * @param fileName
	 */
	public static void clear(Context ctx,String fileName) {
		SharedPreferences sp = ctx.getSharedPreferences(fileName, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.clear();
		editor.commit();
	}
}
