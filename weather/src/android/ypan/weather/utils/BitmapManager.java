package android.ypan.weather.utils;

import java.io.File;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Html.ImageGetter;
import android.widget.ImageView;
import android.ypan.weather.ApiClient;

/**
 * �첽�̼߳���ͼƬ������ ʹ��˵���� BitmapManager bmpManager; bmpManager = new
 * BitmapManager(BitmapFactory.decodeResource(context.getResources(),
 * R.drawable.loading)); bmpManager.loadBitmap(imageURL, imageView);
 * 
 * @author panyingyun 
 * @version 1.0
 * @created 2012-6-25
 */
public class BitmapManager {

	private static HashMap<String, SoftReference<Bitmap>> cache;
	private static ExecutorService pool;
	private static Map<ImageView, String> imageViews;
	private Bitmap defaultBmp;
	private static String SDPATH = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + "/.weather/pic/";;
	static {
		cache = new HashMap<String, SoftReference<Bitmap>>();
		pool = Executors.newFixedThreadPool(5); // �̶��̳߳�
		imageViews = Collections
				.synchronizedMap(new WeakHashMap<ImageView, String>());
	}

	public BitmapManager() {
	}

	public BitmapManager(Bitmap def) {
		this.defaultBmp = def;
	}

	/**
	 * ����Ĭ��ͼƬ
	 * 
	 * @param bmp
	 */
	public void setDefaultBmp(Bitmap bmp) {
		defaultBmp = bmp;
	}

	/**
	 * ����ͼƬ
	 * 
	 * @param url
	 * @param imageView
	 */
	public void loadBitmap(String url, ImageView imageView) {
		loadBitmap(url, imageView, this.defaultBmp, 0, 0);
	}

	/**
	 * ����ͼƬ-�����ü���ʧ�ܺ���ʾ��Ĭ��ͼƬ
	 * 
	 * @param url
	 * @param imageView
	 * @param defaultBmp
	 */
	public void loadBitmap(String url, ImageView imageView, Bitmap defaultBmp) {
		loadBitmap(url, imageView, defaultBmp, 0, 0);
	}

	/**
	 * ����ͼƬ-��ָ����ʾͼƬ�ĸ߿�
	 * 
	 * @param url
	 * @param imageView
	 * @param width
	 * @param height
	 */
	public void loadBitmap(String url, ImageView imageView, Bitmap defaultBmp,
			int width, int height) {
		imageViews.put(imageView, url);
		Bitmap bitmap = getBitmapFromCache(url);

		if (bitmap != null) {
			// ��ʾ����ͼƬ
			imageView.setImageBitmap(bitmap);
		} else {
			// ����SD���е�ͼƬ����
			String filename = FileUtils.getFileName(url);
			String filepath = SDPATH + filename;
			File file = new File(filepath);
			if (file.exists()) {
				// ��ʾSD���е�ͼƬ����
				Bitmap bmp = ImageUtils.PNGToBitmap(filepath);
				imageView.setImageBitmap(bmp);
			} else {
				// �̼߳�������ͼƬ
				imageView.setImageBitmap(defaultBmp);
				queueJob(url, imageView, width, height);
			}
		}
	}

	/**
	 * �ӻ����л�ȡͼƬ
	 * 
	 * @param url
	 */
	public Bitmap getBitmapFromCache(String url) {
		Bitmap bitmap = null;
		if (cache.containsKey(url)) {
			bitmap = cache.get(url).get();
		}
		return bitmap;
	}

	/**
	 * �������м���ͼƬ
	 * 
	 * @param url
	 * @param imageView
	 * @param width
	 * @param height
	 */
	public void queueJob(final String url, final ImageView imageView,
			final int width, final int height) {
		/* Create handler in UI thread. */
		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				String tag = imageViews.get(imageView);
				if (tag != null && tag.equals(url)) {
					if (msg.obj != null) {
						imageView.setImageBitmap((Bitmap) msg.obj);
						String filepath = SDPATH + FileUtils.getFileName(url);
						// ��SD����д��ͼƬ����
						ImageUtils.Bitmap2PNG((Bitmap) msg.obj, filepath);
					}
				}
			}
		};

		pool.execute(new Runnable() {
			public void run() {
				Message message = Message.obtain();
				message.obj = downloadBitmap(url, width, height);
				handler.sendMessage(message);
			}
		});
	}

	/**
	 * ����ͼƬ-��ָ����ʾͼƬ�ĸ߿�
	 * 
	 * @param url
	 * @param width
	 * @param height
	 */
	private Bitmap downloadBitmap(String url, int width, int height) {
		Bitmap bitmap = ApiClient.getNetBitmap(url);
		if (width > 0 && height > 0) {
			// ָ����ʾͼƬ�ĸ߿�
			bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
		}
		// ���뻺��
		cache.put(url, new SoftReference<Bitmap>(bitmap));
		return bitmap;
	}
	
	public static ImageGetter getter= new ImageGetter() {

		@Override
		public Drawable getDrawable(String source) {
			// �ڴ˱����첽����ͼƬ
			Drawable d = null;
			try {
				Bitmap bm=null;
				long start = System.currentTimeMillis();
				String filename = FileUtils.getFileName(source);
				String filepath = SDPATH + filename;
				File file = new File(filepath);
				if (file.exists()) {
					// ��ʾSD���е�ͼƬ����
					bm = ImageUtils.PNGToBitmap(filepath);
				} else {
					// http����ͼƬ
					InputStream is = new DefaultHttpClient()
					.execute(new HttpGet(source)).getEntity()
					.getContent();
					 bm = BitmapFactory.decodeStream(is);
					// ��SD����д��ͼƬ����
					 ImageUtils.Bitmap2PNG(bm, filepath);
				}

				d = new BitmapDrawable(bm);
				d.setBounds(0, 0, bm.getWidth(), bm.getHeight());
				SLog.i("BitmapManager", "d = " + d);
				SLog.i("BitmapManager", "time = " + (System.currentTimeMillis() - start));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return d;
		}
	};
}
