package android.ypan.weather.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImageUtils {
	public static boolean Bitmap2PNG(Bitmap bmp, String filepath) {
		if (bmp == null || filepath == null)
			return false;
		SLog.i("test", "Bitmap2PNG = " + filepath);
		//创建目录和nomedia文件夹
		OutputStream stream = null;
		try {
			File file = new File(filepath);
			File dir = new File(file.getParent());
			if (!dir.exists()) {
				dir.mkdirs();
				File nomedia = new File(dir, ".nomedia");
				if (!nomedia.exists()) {
					try {
						nomedia.createNewFile();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			//删除老的文件 保持新的文件
			if (file.exists()) {
				file.delete();
			}

			stream = new FileOutputStream(filepath);

			if (bmp.compress(Bitmap.CompressFormat.PNG, 85, stream)) {
				stream.flush();
				stream.close();
				return true;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	public static Bitmap PNGToBitmap(String path) {
		File file = new File(path);
		if (!file.exists()) {
			SLog.w("FileUtils", "jpeg cache file is not exits");
			return null;
		}
		SLog.i("test", "PNGToBitmap = " + path);
		Bitmap bm = null;
		BitmapFactory.Options bfoOptions = new BitmapFactory.Options();
		bfoOptions.inDither = false;
		bfoOptions.inPurgeable = true;
		bfoOptions.inInputShareable = true;
		bfoOptions.inTempStorage = new byte[32 * 1024];

		FileInputStream fs = null;
		try {
			fs = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			SLog.i("FileUtils", "icon cache file is not exits");
		}

		try {
			if (fs != null)
				bm = BitmapFactory.decodeFileDescriptor(fs.getFD(), null,
						bfoOptions);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fs != null) {
				try {
					fs.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}

		return bm;
	}
}
