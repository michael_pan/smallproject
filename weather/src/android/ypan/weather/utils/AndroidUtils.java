package android.ypan.weather.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.provider.AlarmClock;

public class AndroidUtils {
	// 获取年月日 时分
	public static String getCurAllTime() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm",
				Locale.SIMPLIFIED_CHINESE);
		Date curDate = new Date(System.currentTimeMillis());
		return format.format(curDate);
	}

	// 获取年月日
	public static String getCurDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日",
				Locale.SIMPLIFIED_CHINESE);
		Date curDate = new Date(System.currentTimeMillis());
		return format.format(curDate);
	}
	
	// 获取月日
	public static String getCurDate2() {
		SimpleDateFormat format = new SimpleDateFormat("MM月dd日",
				Locale.SIMPLIFIED_CHINESE);
		Date curDate = new Date(System.currentTimeMillis());
		return format.format(curDate);
	}

	// 获取时分
	public static String getCurTime() {
		SimpleDateFormat format = new SimpleDateFormat("HH:mm",
				Locale.SIMPLIFIED_CHINESE);
		Date curDate = new Date(System.currentTimeMillis());
		return format.format(curDate);
	}

	// 获取时
	public static String getCurHour() {
		SimpleDateFormat format = new SimpleDateFormat("HH",
				Locale.SIMPLIFIED_CHINESE);
		Date curDate = new Date(System.currentTimeMillis());
		return format.format(curDate);
	}

	// 获取分
	public static String getCurMinute() {
		SimpleDateFormat format = new SimpleDateFormat("mm",
				Locale.SIMPLIFIED_CHINESE);
		Date curDate = new Date(System.currentTimeMillis());
		return format.format(curDate);
	}

	//获取今天星期几
	public static String getCurWeek(){
		return getWeek(System.currentTimeMillis());
	}
	
	// 获取星期几
	public static String getWeek(long time) {
		String week = "星期";
		Date curDate = new Date(time);
		Calendar c = Calendar.getInstance();
		c.setTime(curDate);
		if (c.get(Calendar.DAY_OF_WEEK) == 1) {
			week += "天";
		} else if (c.get(Calendar.DAY_OF_WEEK) == 2) {
			week += "一";
		} else if (c.get(Calendar.DAY_OF_WEEK) == 3) {
			week += "二";
		} else if (c.get(Calendar.DAY_OF_WEEK) == 4) {
			week += "三";
		} else if (c.get(Calendar.DAY_OF_WEEK) == 5) {
			week += "四";
		} else if (c.get(Calendar.DAY_OF_WEEK) == 6) {
			week += "五";
		} else if (c.get(Calendar.DAY_OF_WEEK) == 7) {
			week += "六";
		}
		return week;
	}
	/**
	 * 获取农历日期
	 * @return
	 */
    public static String getChinaDate() {  
        Calendar today = Calendar.getInstance();   
        Date curDate = new Date(System.currentTimeMillis());
        today.setTime(curDate);
        LunarUtils lunar = new LunarUtils(today);  
        return lunar.toString();
    }  
    //获取手机闹钟的Intent
 // 获取闹钟的PendingIntent  
    public static  PendingIntent getClockPendingIntent(Context context) {  
      
        if(Build.VERSION.SDK_INT >=9){  
            Intent intentClock= new Intent();  
            intentClock.setAction(AlarmClock.ACTION_SET_ALARM);  
            return PendingIntent.getActivity(context, 0, intentClock, 0);  
        }  
        SLog.i("test","SDK_INT:" + Build.VERSION.SDK_INT);  
          
        PackageManager packageManager = context.getPackageManager();  
        Intent alarmClockIntent = new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_LAUNCHER);  
        // Verify clock implementation  
        String clockImpls[][] = {  
                {"HTC Alarm Clock", "com.htc.android.worldclock", "com.htc.android.worldclock.WorldClockTabControl" },  
                {"Standar Alarm Clock", "com.android.deskclock", "com.android.deskclock.AlarmClock"},  
                {"Froyo Nexus Alarm Clock", "com.google.android.deskclock", "com.android.deskclock.DeskClock"},  
                {"Moto Blur Alarm Clock", "com.motorola.blur.alarmclock",  "com.motorola.blur.alarmclock.AlarmClock"},  
                {"Samsung Galaxy Clock", "com.sec.android.app.clockpackage","com.sec.android.app.clockpackage.ClockPackage"},  
                {"google 2.1 Clock", "com.android.deskclock","com.android.deskclock.DeskClock"},  
                {"emulator 2.1 Clock", "com.android.alarmclock","com.android.alarmclock.AlarmClock"}  
        };  
      
        boolean foundClockImpl = false;  
      
        for(int i=0; i<clockImpls.length; i++) {  
            String vendor = clockImpls[i][0];  
            String packageName = clockImpls[i][1];  
            String className = clockImpls[i][2];  
            try {  
                ComponentName cn = new ComponentName(packageName, className);  
                ActivityInfo info = packageManager.getActivityInfo(cn, PackageManager.GET_META_DATA);  
                alarmClockIntent.setComponent(cn);  
                SLog.i("test","Found " + vendor + " --> " + packageName + "/" + className);  
                foundClockImpl = true;  
            } catch (NameNotFoundException e) {  
                SLog.i("test",vendor + " does not exists");  
            }  
        }  
      
        if (foundClockImpl) {  
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, alarmClockIntent, 0);  
            return pendingIntent;  
        }else{  
            return null;  
        }  
    } 
}
