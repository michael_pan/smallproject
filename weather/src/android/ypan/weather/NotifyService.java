package android.ypan.weather;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.text.format.DateUtils;
import android.ypan.weather.bean.AllEntry;
import android.ypan.weather.bean.SixDayEntry;
import android.ypan.weather.bean.TodayEntry;
import android.ypan.weather.bean.TodayNowEntry;
import android.ypan.weather.utils.SLog;
import android.ypan.weather.utils.ToastUtils;

public class NotifyService extends Service {
	private static final String TAG = "Weather";
	private static ExecutorService mPool;
	// 监听
	private Handler mCallback;
	// Binder
	private final IBinder mBinder = new NotifyBinder();
	// 刷新标识
	public static final String REFRESH = "refresh";
	public static final String ACTION_REFRESH = "refresh";
	private boolean isRefresh = false;
	//
	private Handler mHandler = new Handler();

	public class NotifyBinder extends Binder {
		NotifyService getService() {
			return NotifyService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mPool = Executors.newSingleThreadExecutor();
		registerAlarmer();
		registerClockReceiver();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		SLog.i(TAG, "startId = " + startId);
		if (intent != null && intent.getExtras() != null) {
			isRefresh = intent.getBooleanExtra(REFRESH, false);
			// SLog.i(TAG, "intent.getExtras() = "+intent.getExtras());
			// SLog.i(TAG, "intent.getAction = "+intent.getAction());
		}
		SLog.i(TAG, "isRefresh = " + isRefresh);
		if (isRefresh) {
			ToastUtils.showUIToast(this, R.string.toast_refresh);
		}
		updateCitys(MainApp.citys);
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterClockReceiver();
		unregisterAlarmer();
		mPool.shutdownNow();
		mPool = null;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}

	// 注册回调
	public void registerCallback(Handler callback) {
		mCallback = callback;
		notifyCitys(MainApp.citys);
	}

	// 反注册
	public void unregisterCallback(Handler callback) {
		mCallback = null;
	}

	// 添加一个城市
	public ArrayList<String> addCity(String city) {
		updateCity(city);
		return MainApp.addCity(city);
	}

	// 通知天气预报
	private void notifyCitys(final ArrayList<String> citys) {
		// SLog.e(TAG, "notifyCity ....");
		if (citys == null || citys.size() == 0) {
			return;
		}
		mPool.execute(new Runnable() {
			@Override
			public void run() {
				for (String city : citys) {
					AllEntry entry = MainApp.getEntry(city);
					sendWeatherMsg(entry);
				}
				updateWidget();
			}
		});
	}

	// 更新并且通知天气预报
	private void updateCitys(final ArrayList<String> citys) {
		// SLog.e(TAG, "updateCitys ....");
		if (mPool == null || citys == null || citys.size() == 0) {
			return;
		}
		mPool.execute(new Runnable() {
			@Override
			public void run() {
				for (String city : citys) {
					AllEntry entry = MainApp.getEntry(city);
					sendWeatherMsg(entry);
					entry = getAllEntry(city);
					MainApp.setEntry(city, entry);
					sendWeatherMsg(entry);

				}
				updateWidget();
			}
		});
	}

	// 添加一个城市后，更新该城市的天气预报
	private void updateCity(final String city) {
		if (city == null)
			return;
		mPool.execute(new Runnable() {
			@Override
			public void run() {
				AllEntry entry = MainApp.getEntry(city);
				sendWeatherMsg(entry);
				entry = getAllEntry(city);
				MainApp.setEntry(city, entry);
				sendWeatherMsg(entry);
				updateWidget();
			}
		});
	}

	// 获取天气
	private AllEntry getAllEntry(String city) {
		String cityid = MainApp.getCityManager().getCityIDByName(city);
		String jsontodaynow = ApiClient.getTodayNowWeather(this, cityid);
		String jsontoday = ApiClient.getTodayWeather(this, cityid);
		String jsonsixday = ApiClient.getFutureWeather(this, cityid);
		TodayNowEntry todayNow = Parser.getTodayNowEntry(jsontodaynow);
		TodayEntry today = Parser.getTodayEntry(jsontoday);
		SixDayEntry future = Parser.getSixDayEntry(jsonsixday);
		if (todayNow == null || today == null || future == null) {
			return null;
		} else {
			return new AllEntry(Integer.valueOf(cityid), todayNow, today,
					future);
		}
	}

	// 向UI发生天气更新指令
	private void sendWeatherMsg(AllEntry entry) {
		if (mCallback != null && entry != null) {
			Message msg = new Message();
			msg.what = NotifyMsg.MSG_ALL;
			msg.obj = entry;
			mCallback.sendMessage(msg);
		}
	}

	// 向UI发生时间更新指令
	private void sendClockMsg() {
		if (mCallback != null) {
			Message msg = new Message();
			msg.what = NotifyMsg.MSG_CLOCK;
			mCallback.sendMessage(msg);
		}
	}

	// 注册Alarm定时任务， 每隔两个小时定时获取天气信息,每秒刷新一次时间
	private static final String ACTION_UPDATE_WEATHER = "android.ypan.weather.alarm";
	private static long WeatherInterval = 2 * DateUtils.HOUR_IN_MILLIS;
	private AlarmManager am;
	private AlarmRecevier mAlarmRecevier;
	private PendingIntent mSenderWeather;

	private class AlarmRecevier extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equalsIgnoreCase(ACTION_UPDATE_WEATHER)) {
				// SLog.e(TAG, "alarm receiver ok!!!!! updateCitys ....");
				updateCitys(MainApp.citys);
			}
		}
	}

	private void registerAlarmer() {

		mAlarmRecevier = new AlarmRecevier();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ACTION_UPDATE_WEATHER);
		registerReceiver(mAlarmRecevier, intentFilter);
		am = (AlarmManager) getSystemService(ALARM_SERVICE);

		// 天气更新定时器
		Intent intent = new Intent(ACTION_UPDATE_WEATHER);
		mSenderWeather = PendingIntent.getBroadcast(this, 0, intent, 0);
		long firstWeather = SystemClock.elapsedRealtime();
		firstWeather += WeatherInterval;
		if (am != null) {
			am.setRepeating(AlarmManager.ELAPSED_REALTIME, firstWeather,
					WeatherInterval, mSenderWeather);
		}

	}

	private void unregisterAlarmer() {

		if (mAlarmRecevier != null) {
			unregisterReceiver(mAlarmRecevier);
			mAlarmRecevier = null;
		}

		if (mSenderWeather != null && am != null) {
			am.cancel(mSenderWeather);
		}
	}

	// 时间更新监听
	private ClockRecevier mClockReceiver = null;

	private class ClockRecevier extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// SLog.i(TAG, "ClockRecevier action = " + intent.getAction());
			sendClockMsg();
			// 更新widget
			updateWidget();
		}
	}

	private void registerClockReceiver() {
		IntentFilter clockFilter = new IntentFilter();
		clockFilter.addAction(Intent.ACTION_TIME_TICK);
		clockFilter.addAction(Intent.ACTION_TIME_CHANGED);
		clockFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
		mClockReceiver = new ClockRecevier();
		registerReceiver(mClockReceiver, clockFilter);
	}

	private void unregisterClockReceiver() {
		if (mClockReceiver != null) {
			unregisterReceiver(mClockReceiver);
			mClockReceiver = null;
		}
	}

	// 更新杭州天气的广播
	private void updateWidget() {
		Intent intent = new Intent();
		intent.setAction(WeatherWidgetProvider.ACTION_UPDATE_WIDGET_WEATHER);
		sendBroadcast(intent);
		if (isRefresh) {
			mHandler.post(new Runnable() {

				@Override
				public void run() {
					ToastUtils.showUIToast(NotifyService.this,
							R.string.toast_refresh_success);
				}
			});
			isRefresh = false;
		}
	}
}
