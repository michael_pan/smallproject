package android.ypan.weather.bean;

import java.io.Serializable;

import android.ypan.weather.Parser;

public class AllEntry implements Serializable {

	private static final long serialVersionUID = 5031382315617496066L;
	//城市ID
	private int mCityID;
	//今日天气
	private TodayNowEntry mTodayNow;
	//
	private TodayEntry mToday;
	//未来天气
	private SixDayEntry mFuture;

	public AllEntry(int cityid,TodayNowEntry todayNow, TodayEntry today, SixDayEntry future) {
		mCityID = cityid;
		mTodayNow = todayNow;
		mToday = today;
		mFuture = future;
	}
	
	public int getCityID(){
		return mCityID;
	}

	public TodayNowEntry getTodayNow() {
		return mTodayNow;
	}
	
	public void setTodayNow(TodayNowEntry todaynow){
		mTodayNow = todaynow;
	}

	public SixDayEntry getFuture() {
		return mFuture;
	}

	public void setFuture(SixDayEntry future){
		mFuture = future;
	}

	public TodayEntry getToday() {
		return mToday;
	}
	
	public void setToday(TodayEntry today){
		mToday = today;
	}
}
