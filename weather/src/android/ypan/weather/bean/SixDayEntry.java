package android.ypan.weather.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class SixDayEntry implements Serializable{
	private static final long serialVersionUID = -6540201201274812184L;
	public static final String INFO = "weatherinfo";
	public static final String CITY = "city";
	public static final String CITYID = "cityid";
	public static final String TEMP = "temp";
	public static final String WEATHER = "weather";
	public static final String IMG = "img";
	public static final String WIND = "wind";
	
	private String mCity;
	private String mCityID;
	private ArrayList<String> mTempList;
	private ArrayList<String> mWeatherList;
	private ArrayList<String> mImageUrlList;
	private ArrayList<String> mWindList;
	
	public String getCity() {
		return mCity;
	}
	public void setCity(String city) {
		this.mCity = city;
	}
	public String getCityID() {
		return mCityID;
	}
	public void setCityID(String cityID) {
		this.mCityID = cityID;
	}
	
	public ArrayList<String> getTempList() {
		return mTempList;
	}
	
	public void setTempList(ArrayList<String> tempList) {
		this.mTempList = tempList;
	}
	public ArrayList<String> getWeatherList() {
		return mWeatherList;
	}
	public void setWeatherList(ArrayList<String> weatherList) {
		this.mWeatherList = weatherList;
	}
	public ArrayList<String> getImageUrlList() {
		return mImageUrlList;
	}
	public void setImageUrlList(ArrayList<String> imageUrlList) {
		this.mImageUrlList = imageUrlList;
	}
	public ArrayList<String> getWindList() {
		return mWindList;
	}
	public void setWindList(ArrayList<String> windList) {
		this.mWindList = windList;
	}
}

