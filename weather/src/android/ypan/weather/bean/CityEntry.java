package android.ypan.weather.bean;

/**
 * city entry
 * @author Administrator
 *
 */
public class CityEntry {
	private String name;
	private String pinyin;
	private String id;
	
	public CityEntry(String name, String pinyin, String id){
		this.name = name;
		this.pinyin = pinyin;
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPinyin() {
		return pinyin;
	}
	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}
	public String getID() {
		return id;
	}
	public void setID(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "name = "+name+",pinyin = "+pinyin+",id = "+id;
	}

}
