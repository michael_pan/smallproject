package android.ypan.weather.bean;

import java.io.Serializable;

/**
 * 当日实时天气
 * @author Administrator
 *
 */
public class TodayNowEntry implements Serializable{
	private static final long serialVersionUID = -3429993891571178138L;
	public static final String INFO = "weatherinfo";
	public static final String CITY = "city";
	public static final String CITYID = "cityid";
	public static final String TEMP = "temp";
	public static final String WIND="WD";
    public static final String WS="WS";
    public static final String SD="SD";
    public static final String WSE="WSE";
    public static final String TIME="time";
    
    private String city;   //城市
    private String cityid; //城市ID
    private String temp;   //温度
    private String wd;     //风向
    private String ws;     //风级
    private String sd;     //湿度
    private String time;   //更新时间
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCityid() {
		return cityid;
	}
	
	public void setCityid(String cityid) {
		this.cityid = cityid;
	}
	
	public String getTemp() {
		return temp;
	}
	
	public void setTemp(String temp) {
		this.temp = temp;
	}
	
	public String getWd() {
		return wd;
	}
	
	public void setWd(String wd) {
		this.wd = wd;
	}
	
	public String getWs() {
		return ws;
	}
	
	public void setWs(String ws) {
		this.ws = ws;
	}
	
	public String getSd() {
		return sd;
	}
	
	public void setSd(String sd) {
		this.sd = sd;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	
	@Override
	public String toString() {
		return "city = "+city+",cityid = "+cityid+",temp = "+temp+",time"+time;
	}
}
