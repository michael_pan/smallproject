package android.ypan.weather.bean;

import java.io.Serializable;


public class TodayEntry implements Serializable{
	private static final long serialVersionUID = -1595999519548412464L;
	public static final String INFO = "weatherinfo";
	public static final String CITY = "city";
	public static final String CITYID = "cityid";
	public static final String TEMPL = "temp1";
	public static final String TEMPH = "temp2";
	public static final String WEATHER = "weather";
	public static final String IMG1 = "img1"; //夜间，天气图片
	public static final String IMG2 = "img2"; //白天，天气图片
	public static final String PTIME = "ptime";
	
	private String city; //城市
	private String cityid; //城市ID
	private String highTemp; //最高温度
	private String lowTemp; //最低温度
	private String weather; //天气文字描述
	private String nightImage; //夜间天气图片
	private String dayImage; //白天天气图片
	private String lastUpTime; //最后更新时间
	
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	public String getCityid() {
		return cityid;
	}
	public void setCityid(String cityid) {
		this.cityid = cityid;
	}
	
	public String getHighTemp() {
		return highTemp;
	}
	public void setHighTemp(String highTemp) {
		this.highTemp = highTemp;
	}
	
	public String getLowTemp() {
		return lowTemp;
	}
	public void setLowTemp(String lowTemp) {
		this.lowTemp = lowTemp;
	}
	
	public String getWeather() {
		return weather;
	}
	public void setWeather(String weather) {
		this.weather = weather;
	}
	
	public String getNightImage() {
		return nightImage;
	}
	public void setNightImage(String nightImage) {
		this.nightImage = nightImage;
	}
	
	public String getDayImage() {
		return dayImage;
	}
	public void setDayImage(String dayImage) {
		this.dayImage = dayImage;
	}
	public String getLastUpdateTime() {
		return lastUpTime;
	}
	public void setLastUpdateTime(String lastUpTime) {
		this.lastUpTime = lastUpTime;
	}
	
	@Override
	public String toString() {
		return "city = "+city+",cityid = "+cityid+",highTemp = "+highTemp+",lowTemp = "+lowTemp+",lastUpTime = "+lastUpTime;
	}
}
