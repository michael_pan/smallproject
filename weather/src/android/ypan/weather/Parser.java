package android.ypan.weather;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.ypan.weather.bean.SixDayEntry;
import android.ypan.weather.bean.TodayEntry;
import android.ypan.weather.bean.TodayNowEntry;
import android.ypan.weather.utils.SLog;

public class Parser {

	public static final String TAG = Parser.class.getSimpleName();
	private static final String IMG_Head = "http://m.weather.com.cn/img/b";
	private static final String IMG_Tail = ".gif";

	public static TodayNowEntry getTodayNowEntry(String jsonStr) {
		TodayNowEntry entry = new TodayNowEntry();
		try {
			JSONObject obj = new JSONObject(jsonStr);
			JSONObject objinfo = (JSONObject) obj.get(TodayNowEntry.INFO);
			entry.setCity((String) objinfo.getString(TodayNowEntry.CITY));
			entry.setCityid((String) objinfo.getString(TodayNowEntry.CITYID));
			entry.setSd((String) objinfo.getString(TodayNowEntry.SD));
			entry.setTemp((String) objinfo.getString(TodayNowEntry.TEMP));
			entry.setWd((String) objinfo.getString(TodayNowEntry.WIND));
			entry.setWs((String) objinfo.getString(TodayNowEntry.WS));
			entry.setTime((String) objinfo.getString(TodayNowEntry.TIME));
		} catch (JSONException e) {
			SLog.e(TAG, "getTodayNowEntry jsonobject convert error");
			entry = null;
		}
		// Log.d(TAG, "entry = " + entry.toString());
		return entry;
	}

	public static TodayEntry getTodayEntry(String jsonStr) {
		TodayEntry entry = new TodayEntry();
		try {
			JSONObject obj = new JSONObject(jsonStr);
			JSONObject objinfo = (JSONObject) obj.get(TodayEntry.INFO);
			entry.setCity((String) objinfo.getString(TodayEntry.CITY));
			entry.setCityid((String) objinfo.getString(TodayEntry.CITYID));
			entry.setNightImage((String) objinfo.getString(TodayEntry.IMG1));
			entry.setDayImage((String) objinfo.getString(TodayEntry.IMG2));
			entry.setHighTemp((String) objinfo.getString(TodayEntry.TEMPH));
			entry.setLowTemp((String) objinfo.getString(TodayEntry.TEMPL));
			entry.setLastUpdateTime((String) objinfo
					.getString(TodayEntry.PTIME));
			entry.setWeather((String) objinfo.getString(TodayEntry.WEATHER));
		} catch (JSONException e) {
			SLog.e(TAG, "getTodayEntry jsonobject convert error");
			entry = null;
		}
		// SLog.d(TAG, "entry = " + entry.toString());
		return entry;
	}

	public static SixDayEntry getSixDayEntry(String jsonStr) {
		SixDayEntry entry = new SixDayEntry();
		try {
			JSONObject obj = new JSONObject(jsonStr);
			JSONObject objinfo = (JSONObject) obj.get(SixDayEntry.INFO);
			entry.setCity(objinfo.getString(SixDayEntry.CITY));
			entry.setCity(objinfo.getString(SixDayEntry.CITYID));
			ArrayList<String> tempList = new ArrayList<String>();
			tempList.add(objinfo.getString(SixDayEntry.TEMP + "1"));
			tempList.add(objinfo.getString(SixDayEntry.TEMP + "2"));
			tempList.add(objinfo.getString(SixDayEntry.TEMP + "3"));
			tempList.add(objinfo.getString(SixDayEntry.TEMP + "4"));
			tempList.add(objinfo.getString(SixDayEntry.TEMP + "5"));
			tempList.add(objinfo.getString(SixDayEntry.TEMP + "6"));
			entry.setTempList(tempList);
			ArrayList<String> weatherList = new ArrayList<String>();
			weatherList.add(objinfo.getString(SixDayEntry.WEATHER + "1"));
			weatherList.add(objinfo.getString(SixDayEntry.WEATHER + "2"));
			weatherList.add(objinfo.getString(SixDayEntry.WEATHER + "3"));
			weatherList.add(objinfo.getString(SixDayEntry.WEATHER + "4"));
			weatherList.add(objinfo.getString(SixDayEntry.WEATHER + "5"));
			weatherList.add(objinfo.getString(SixDayEntry.WEATHER + "6"));
			entry.setWeatherList(weatherList);
			ArrayList<String> imgUrlList = new ArrayList<String>();
			imgUrlList.add(IMG_Head + objinfo.getString(SixDayEntry.IMG + "1")
					+ IMG_Tail);
			imgUrlList.add(IMG_Head + objinfo.getString(SixDayEntry.IMG + "3")
					+ IMG_Tail);
			imgUrlList.add(IMG_Head + objinfo.getString(SixDayEntry.IMG + "5")
					+ IMG_Tail);
			imgUrlList.add(IMG_Head + objinfo.getString(SixDayEntry.IMG + "7")
					+ IMG_Tail);
			imgUrlList.add(IMG_Head + objinfo.getString(SixDayEntry.IMG + "9")
					+ IMG_Tail);
			imgUrlList.add(IMG_Head + objinfo.getString(SixDayEntry.IMG + "11")
					+ IMG_Tail);
			entry.setImageUrlList(imgUrlList);
			ArrayList<String> windList = new ArrayList<String>();
			windList.add(objinfo.getString(SixDayEntry.WIND + "1"));
			windList.add(objinfo.getString(SixDayEntry.WIND + "2"));
			windList.add(objinfo.getString(SixDayEntry.WIND + "3"));
			windList.add(objinfo.getString(SixDayEntry.WIND + "4"));
			windList.add(objinfo.getString(SixDayEntry.WIND + "5"));
			windList.add(objinfo.getString(SixDayEntry.WIND + "6"));
			entry.setWindList(windList);
		} catch (JSONException e) {
			SLog.e(TAG, "getTodayEntry jsonobject convert error");
			entry = null;
		}
		// SLog.d(TAG, "entry = " + entry.toString());
		return entry;
	}
}
