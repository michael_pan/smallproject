package android.ypan.weather;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.ypan.weather.utils.SLog;
/**
 * start service when device boot
 * @author Administrator
 *
 */
public class DeviceBootReceiver extends BroadcastReceiver {
    private static final String TAG = "DeviceBootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            // start weather service
            Intent intentService = new Intent(context, NotifyService.class);
            context.startService(intentService);
            SLog.i(TAG, "Start NotifyService when device boot ********");
        }
    }
}
