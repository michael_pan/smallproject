package android.ypan.weather;

import java.util.HashMap;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.ypan.weather.bean.AllEntry;
import android.ypan.weather.bean.TodayEntry;
import android.ypan.weather.bean.TodayNowEntry;
import android.ypan.weather.utils.AndroidUtils;

//参考文献1：
//http://www.itzhai.com/android-app-widget-desktop-components-to-use-appwidgetprovider-remoteviews.html
//参考文献2：
//http://blog.csdn.net/thl789/article/details/7887968
//参考文献3：
//http://blog.csdn.net/iefreer/article/details/4626274
public class WeatherWidgetProvider extends AppWidgetProvider {
	public static final String ACTION_UPDATE_WIDGET_WEATHER = "android.ypan.action.UPDATE_WEATHER";
	private static final String TAG = "WeatherWidgetProvider";
	private HashMap<Integer, RemoteViews> mapRemoteViews = new HashMap<Integer, RemoteViews>();

	// 接收广播事件
	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		// SLog.i(TAG, "onReceive");
		String action = intent.getAction();
		if (ACTION_UPDATE_WIDGET_WEATHER.equals(action)) {
			ComponentName componentName = new ComponentName(context,
					WeatherWidgetProvider.class);
			AppWidgetManager appWidgetManager = AppWidgetManager
					.getInstance(context);
			int[] appWidgetIds = appWidgetManager
					.getAppWidgetIds(componentName);
			updateAllWidget(context, appWidgetManager, appWidgetIds);
		}

	}

	// 到达指定更新时间或用户向桌面添加了App Widget时调用此方法
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		// SLog.i(TAG, "onUpdate");
		updateAllWidget(context, appWidgetManager, appWidgetIds);
	}

	// 删除App Widget是调用此方法,做一个删除工作
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		// SLog.i(TAG, "onDeleted");
	}

	// App WIdget实例第一次被创建是调用此方法
	@Override
	public void onEnabled(Context context) {
		// SLog.i(TAG, "onEnabled");
	}

	// 最后一个App Widget实例删除后调用此方法
	@Override
	public void onDisabled(Context context) {
		// SLog.i(TAG, "onDisabled");
	}

	private void updateAllWidget(Context context, AppWidgetManager mgr,
			int[] appWidgetIds) {
		if (MainApp.citys.size() <= 0)
			return;
		final int len = appWidgetIds.length;
		for (int i = 0; i < len; i++) {
			RemoteViews remoteViews = getRemoveViews(context, appWidgetIds[i]);
			updateData(context, MainApp.getEntry(MainApp.citys.get(0)),
					remoteViews);
			mgr.updateAppWidget(appWidgetIds[i], remoteViews);
		}
	}

	private void updateData(Context context, AllEntry entry,
			RemoteViews remoteViews) {
		if (context == null || remoteViews == null)
			return;
		if (entry != null) {
			TodayNowEntry todaynow = entry.getTodayNow();
			// SixDayEntry future = entry.getFuture();
			TodayEntry today = entry.getToday();

			// 城市
			remoteViews.setTextViewText(R.id.widget_location,
					todaynow.getCity());
			// 温度
			// String highTemp = today.getHighTemp();
			// String lowTemp = today.getLowTemp();
			String curTemp = todaynow.getTemp();
			String temp = context.getResources().getString(
					R.string.widget_temp, curTemp);
			remoteViews.setTextViewText(R.id.widget_temperature, temp);

			// 天气+风力
			remoteViews.setTextViewText(R.id.widget_description,
					today.getWeather());
			remoteViews.setTextViewText(R.id.widget_wind, todaynow.getWd()
					+ todaynow.getWs());
			// 更新时间
			remoteViews.setTextViewText(R.id.last_update_time_stamp, context
					.getResources().getString(R.string.item_updatetime)
					+ todaynow.getTime());

		}
		// 阳历+星期+阴历
		String date = AndroidUtils.getCurDate2() + " "
				+ AndroidUtils.getCurWeek() + " " + AndroidUtils.getChinaDate();
		remoteViews.setTextViewText(R.id.local_date, date);
		remoteViews.setTextViewText(R.id.widget_4x2_local_time_hours,
				AndroidUtils.getCurHour());
		remoteViews.setTextViewText(R.id.widget_4x2_local_time_mins,
				AndroidUtils.getCurMinute());

		// PendingIntent为事件触发是所要执行的PendingIntent
		PendingIntent pendingIntent_w = getWeatherPendingIntent(context);
		remoteViews.setOnClickPendingIntent(R.id.widget_4x2_weather,
				pendingIntent_w);
		// 闹钟设置
		PendingIntent pendingIntent_c = AndroidUtils
				.getClockPendingIntent(context);
		remoteViews.setOnClickPendingIntent(R.id.widgetdigitalclock,
				pendingIntent_c);
		// 刷新
		PendingIntent pendingIntent_r = getRefreshPendingIntent(context);
		remoteViews.setOnClickPendingIntent(R.id.widget_refresh,
				pendingIntent_r);
	}

	private RemoteViews getRemoveViews(Context ctx, Integer appWidgetId) {
		RemoteViews views = mapRemoteViews.get(appWidgetId);
		if (views == null) {
			views = new RemoteViews(ctx.getPackageName(),
					R.layout.weather_widget);
		}
		return views;
	}

	// 获取天气主页的Intent
	private PendingIntent getWeatherPendingIntent(Context context) {
		Intent weather = new Intent(context, WeatherActivity.class);
		return PendingIntent.getActivity(context, 0, weather, 0);
	}

	// 刷新天气的Intent
	private PendingIntent getRefreshPendingIntent(Context context) {
		Intent refresh = new Intent(context, NotifyService.class);
		refresh.putExtra(NotifyService.REFRESH, true);
		refresh.setAction(NotifyService.ACTION_REFRESH);
		return PendingIntent.getService(context, 0, refresh, 0);
	}
}
