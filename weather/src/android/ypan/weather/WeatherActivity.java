package android.ypan.weather;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ListView;
import android.ypan.weather.bean.AllEntry;
import android.ypan.weather.utils.DialogHelper;
import android.ypan.weather.utils.SLog;
import android.ypan.weather.utils.ToastUtils;

import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;

public class WeatherActivity extends ActionBarActivity {
	private static final String TAG = "WeatherActivity";
	private Context mCtx;
	private ListView mListView;
	private WeatherListAdapter mWeatherListAdapter;
	private NotifyService mNotifyService = null;
	Integer lock = 0;
	// 等待对话框
	private Dialog waitingDlg;
	// Options Menu's IDs
	public static final int MENU_OPT = Menu.FIRST;
	public static final int MENU_ABOUT = MENU_OPT + 1; // 关于（查看版本信息）
	public static final int MENU_CITYMGR = MENU_OPT + 2; // 城市管理

	// 和服务建立连接
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			synchronized (lock) {
				mNotifyService = ((NotifyService.NotifyBinder) service)
						.getService();
				lock.notify();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mNotifyService = null;

		}
	};

	private final Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case NotifyMsg.MSG_BINDOK:
				// 如果没有城市，则自动定位城市
				if (MainApp.citys.size() == 0) {
					MainApp.getInstance().getLBSCity(mHandler);
					waitingDlg.show();
					// ToastUtils.showUIToast(mCtx, R.string.toast_lbs_start);
				}
				break;
			case NotifyMsg.MSG_ALL:
				AllEntry entry = (AllEntry) msg.obj;
				mWeatherListAdapter.updateWeather(entry);
				break;
			case NotifyMsg.MSG_CLOCK:
				mWeatherListAdapter.updateTime();
				break;
			case NotifyMsg.MSG_LBS:
				// 定位成功
				String city = (String) msg.obj;
				SLog.i(TAG, "weather city = " + city);
				if (!TextUtils.isEmpty(city)) {
					if (MainApp.getCityManager().getCityIDByName(city) != null) {
						ToastUtils
								.showUIToast(mCtx, R.string.toast_lbs_success);
						mNotifyService.addCity(city);
					} else {
						String toast = getResources().getString(
								R.string.toast_lbs_notsupport, city);
						ToastUtils.showUIToast(mCtx, toast);

					}
				} else {
					ToastUtils.showUIToast(mCtx, R.string.toast_lbs_fail);

				}
				if (waitingDlg != null) {
					waitingDlg.dismiss();
				}
				break;
			default:
				break;
			}
			super.handleMessage(msg);
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.main);
		mCtx = this;
		waitingDlg = DialogHelper.createWaitingDialog(this,
				R.string.dlg_lbs_start);
		initview();
		initactionbar();
		initNotifyCallback();
		// 友盟SDK
		MobclickAgent.onError(this);
		UmengUpdateAgent.setUpdateOnlyWifi(false);
		UmengUpdateAgent.update(this);
	}

	@Override
	protected void onDestroy() {
		if (mNotifyService != null) {
			mNotifyService.unregisterCallback(mHandler);
			getApplicationContext().unbindService(mConnection);
		}
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		MainApp.sMainActivity = this;
		// 友盟SDK
		MobclickAgent.onResume(this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// 友盟SDK
		MobclickAgent.onPause(this);
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();
		MenuItem item;
		item = menu.add(0, MENU_CITYMGR, 0, R.string.menu_city_manager);
		MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
		item = menu.add(0, MENU_ABOUT, 0, R.string.menu_about);
		MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_ABOUT:
			DialogHelper.showAboutDialog().show();
			break;
		case MENU_CITYMGR:
			// showAlertDialog(DIALOG_SORT);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void initview() {
		mListView = (ListView) findViewById(R.id.weather_listview);
		mWeatherListAdapter = new WeatherListAdapter(this, getLayoutInflater());
		mListView.setAdapter(mWeatherListAdapter);
	}

	private void initactionbar() {
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowHomeEnabled(true);
		Drawable bg = getResources().getDrawable(R.drawable.header);
		actionBar.setBackgroundDrawable(bg);
	}

	private void initNotifyCallback() {
		if (mNotifyService == null) {
			Intent intent = new Intent(this, NotifyService.class);
			getApplicationContext().bindService(intent, mConnection,
					Context.BIND_AUTO_CREATE);
		}
		// 注册天气回调接口
		new Thread(new Runnable() {

			@Override
			public void run() {
				synchronized (lock) {
					if (mNotifyService == null) {
						try {
							lock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}

				mNotifyService.registerCallback(mHandler);
				mHandler.sendEmptyMessage(NotifyMsg.MSG_BINDOK);
			}
		}).start();
	}
}
