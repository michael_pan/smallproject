package com.ypan.monitor.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ClassName: OwnDateUtils
 * @Description: my date utils
 * @author Michael.Pan
 * @date 2012-6-18 下午02:15:49
 */
public class OwnDateUtils {

	/***
	 * get current time string
	 */
	public static String getCurrentYMDHMS() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date nowc = new Date();
		String pid = formatter.format(nowc);
		return pid;
	}
}
