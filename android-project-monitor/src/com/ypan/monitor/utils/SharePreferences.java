package com.ypan.monitor.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.ypan.monitor.MainApp;

/**
 * @ClassName: SharePreferences
 * @Description:保持配置
 * @author Michael.Pan
 * @date 2012-6-18 下午06:34:52
 */
public class SharePreferences {

	private static final String UNINSTALL_SHARED_PREFERENCES = "uninstall_share_preferences";

	private static final String HAS_LOGIN_STATUS = "has_login_status";
	private static final String SORT_TYPE = "sort_type";
	private static final String BACKUP_PATH = "backup_path";
	private static final String HAS_ROOT = "has_root";

	/**
	 * 保存是否登录过状态
	 * 
	 * @param context
	 * @param status
	 */
	public static void setHasLoginedStatus(boolean status) {
		saveData(UNINSTALL_SHARED_PREFERENCES, HAS_LOGIN_STATUS, status);
	}

	/**
	 * 获取是否登录过状态
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getHasLoginedStatus() {
		return (Boolean) getData(UNINSTALL_SHARED_PREFERENCES,
				HAS_LOGIN_STATUS, false);
	}

	/**
	 * 保存排序类型
	 * 
	 * @param context
	 * @param status
	 */
	public static void setSortType(int sortType) {
		saveData(UNINSTALL_SHARED_PREFERENCES, SORT_TYPE, sortType);
	}

	/**
	 * 获取保存排序类型
	 * 
	 * @param context
	 * @return
	 */
	public static int getSortType() {
		return (Integer) getData(UNINSTALL_SHARED_PREFERENCES, SORT_TYPE,
				Define.SORT_BY_TIME_DESC);
	}

	/**
	 * 保持备份路径
	 */
	public static void setBackupPath(String path) {
		saveData(UNINSTALL_SHARED_PREFERENCES, BACKUP_PATH, path);
	}

	/**
	 * 获取备份路径
	 */
	public static String getBackupPath() {
		return (String) getData(UNINSTALL_SHARED_PREFERENCES, BACKUP_PATH,
				Define.BACKUP_APK_PATH);
	}

	/**
	 * 设置是否获取root权限
	 */
	public static void setHasRoot(boolean hasRoot) {
		saveData(UNINSTALL_SHARED_PREFERENCES, HAS_ROOT, hasRoot);
	}

	/**
	 * 获取是否获取root权限
	 */
	public static boolean getHasRoot() {
		return (Boolean) getData(UNINSTALL_SHARED_PREFERENCES, HAS_ROOT, true);
	}

	/**
	 * 保存数据
	 * 
	 * @param context
	 * @param fileName
	 * @param key
	 * @param value
	 */
	private static void saveData(String fileName, String key, Object value) {
		SharedPreferences sp = MainApp.sInstance.getSharedPreferences(fileName,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		if (value instanceof Boolean) {
			editor.putBoolean(key, (Boolean) value);
		} else if (value instanceof Integer) {
			editor.putInt(key, (Integer) value);
		} else if (value instanceof Long) {
			editor.putLong(key, (Long) value);
		} else if (value instanceof Float) {
			editor.putFloat(key, (Float) value);
		} else {
			if (value == null) {
				editor.putString(key, "");
			} else {
				editor.putString(key, String.valueOf(value));
			}
		}
		editor.commit();
	}

	/**
	 * 取值
	 * 
	 * @param context
	 * @param fileName
	 * @param key
	 * @param defValue
	 * @return
	 */
	private static Object getData(String fileName, String key, Object defValue) {
		SharedPreferences sp = MainApp.sInstance.getSharedPreferences(fileName,
				Context.MODE_PRIVATE);
		if (defValue instanceof Boolean) {
			return sp.getBoolean(key, (Boolean) defValue);
		} else if (defValue instanceof Integer) {
			return sp.getInt(key, (Integer) defValue);
		} else if (defValue instanceof Long) {
			return sp.getLong(key, (Long) defValue);
		} else if (defValue instanceof Float) {
			return sp.getFloat(key, (Float) defValue);
		} else {
			if (defValue == null) {
				return sp.getString(key, "");
			}
			return sp.getString(key, String.valueOf(defValue));
		}
	}

}
