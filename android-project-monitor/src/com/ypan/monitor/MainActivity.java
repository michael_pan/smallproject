package com.ypan.monitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;

import com.viewpagerindicator.TabPageIndicator;
import com.ypan.monitor.GL.GLSurfaceActivity;
import com.ypan.monitor.osutils.Battery;
import com.ypan.monitor.osutils.Cpu;
import com.ypan.monitor.osutils.GPU;
import com.ypan.monitor.osutils.MainBoard;
import com.ypan.monitor.osutils.Sensors;
import com.ypan.monitor.views.BatteryFragment;

/**
 * @ClassName: MainActivity
 * @Description: 主Activity
 * @author Administrator
 * @date 2012-8-7 下午10:25:09
 */
public class MainActivity extends ActionBarActivity implements
		OnPageChangeListener {

	private static final String TAG = "Monitor";
	private static final int OffscreenPageNumber = 6;
	TabPageIndicator mTabIndicator;
	ViewPager mViewPager;
	MainFragmentAdapter mFragAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_tab);
		initData();
		initView(savedInstanceState);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterBatteryReceiver();
	}

	@Override
	protected void onResume() {
		super.onResume();
		MainApp.sMainActivity = this;
		Sensors.getInstance().onResume(this);
		// 初始化数据
		BaseFragment curFragment = getCurFragment();
		if (curFragment != null)
			curFragment.onLoadData(true);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Sensors.getInstance().onPause(this);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	private void initData() {
		registerBatteryReceiver();
		Cpu.getInstance(this).init();
		GPU.getInstance(this);
		MainBoard.getInstance();
		if (GPU.getInstance(this).getRender() == null) {
			startActivity(new Intent(this, GLSurfaceActivity.class));
		}
		Sensors.getInstance().initSensors(this);
	}

	private void initView(Bundle savedInstanceState) {

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mFragAdapter = new MainFragmentAdapter(this,
				getSupportFragmentManager());
		mViewPager.setAdapter(mFragAdapter);
		mViewPager.setOffscreenPageLimit(OffscreenPageNumber);
		mTabIndicator = (TabPageIndicator) findViewById(R.id.titles);
		mTabIndicator.setViewPager(mViewPager);
		mTabIndicator.setOnPageChangeListener(this);
		Drawable bg = (Drawable) getResources().getDrawable(
				R.drawable.action_bar_bg);
		getSupportActionBar().setBackgroundDrawable(bg);
	}

	@Override
	public void onPageScrollStateChanged(int pos) {

	}

	@Override
	public void onPageScrolled(int pos, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int pos) {
		BaseFragment curFragment = (BaseFragment) mFragAdapter.getItem(pos);
		curFragment.onLoadData(false);
		// Log.e(TAG, "onPageSelected pos = " + pos);
	}

	// 电池监控
	private BatteryReceiver receiver = null;

	private void registerBatteryReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_BATTERY_CHANGED);
		receiver = new BatteryReceiver();
		registerReceiver(receiver, filter);// 注册BroadcastReceiver
	}

	private void unregisterBatteryReceiver() {
		if (receiver != null)
			unregisterReceiver(receiver);
		receiver = null;
	}

	private class BatteryReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			Battery battery = Battery.getInstance(context);
			battery.health = intent.getIntExtra("health", 0);
			battery.level = intent.getIntExtra("level", 0);
			battery.pluged = intent.getIntExtra("plugged", 0);
			battery.scale = intent.getIntExtra("scale", 0);
			battery.status = intent.getIntExtra("status", 0);
			battery.technology = intent.getExtras().getString("technology");
			battery.temp = intent.getIntExtra("temperature", 0);
			battery.voltage = intent.getIntExtra("voltage", 0);
			BaseFragment curFragment = getCurFragment();
			if (curFragment != null && curFragment instanceof BatteryFragment) {
				curFragment.onLoadData(true);
			}
		}
	}

	private BaseFragment getCurFragment() {
		if (mViewPager == null || mFragAdapter == null)
			return null;
		int curPos = mViewPager.getCurrentItem();
		return (BaseFragment) mFragAdapter.getItem(curPos);
	}

}
