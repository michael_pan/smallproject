package com.ypan.monitor.GL;

import android.app.Activity;
import android.os.Bundle;

public class GLSurfaceActivity extends Activity{
    private BasicGLSurfaceView mView;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        mView = new BasicGLSurfaceView(this);
        setContentView(mView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mView.onResume();
    }
}
