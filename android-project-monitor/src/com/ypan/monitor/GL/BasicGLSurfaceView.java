package com.ypan.monitor.GL;

import android.content.Context;
import android.opengl.GLSurfaceView;

public class BasicGLSurfaceView extends GLSurfaceView{

	public BasicGLSurfaceView(Context context) {
		super(context);
        setEGLContextClientVersion(2);
        setRenderer(new BasicRenderer(context));
	}

}
