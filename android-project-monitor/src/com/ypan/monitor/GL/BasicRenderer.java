package com.ypan.monitor.GL;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import com.ypan.monitor.osutils.GPU;

public class BasicRenderer implements GLSurfaceView.Renderer {

	private Context mContext;

	public BasicRenderer(Context context) {
		mContext = context;
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		GPU.getInstance(mContext).renderer = GLES20.glGetString(GLES20.GL_RENDERER);
		GPU.getInstance(mContext).verdor = GLES20.glGetString(GLES20.GL_VENDOR);
		((Activity) mContext).setResult(-1);
		((Activity) mContext).finish();
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {

	}

	@Override
	public void onDrawFrame(GL10 gl) {

	}

}
