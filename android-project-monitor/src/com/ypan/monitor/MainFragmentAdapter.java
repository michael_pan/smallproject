package com.ypan.monitor;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ypan.monitor.views.AboutFragment;
import com.ypan.monitor.views.BatteryFragment;
import com.ypan.monitor.views.BoardFragment;
import com.ypan.monitor.views.CPUFragment;
import com.ypan.monitor.views.GPUFragment;
import com.ypan.monitor.views.SensorsFragment;

public class MainFragmentAdapter extends FragmentPagerAdapter {
	private ArrayList<Integer> mTitleList;
	private Context mCtx;
	private ArrayList<BaseFragment> mFragments = new ArrayList<BaseFragment>();

	public MainFragmentAdapter(Context ctx, FragmentManager fm) {
		super(fm);
		mCtx = ctx;
		mTitleList = new ArrayList<Integer>();
		mTitleList.add(0, R.string.tab_title_cpu);
		mTitleList.add(1, R.string.tab_title_board);
		mTitleList.add(2, R.string.tab_title_gpu);
		mTitleList.add(3, R.string.tab_title_battery);
		mTitleList.add(4, R.string.tab_title_sensors);
		mTitleList.add(5, R.string.tab_title_about);
		mFragments.add(0, CPUFragment.newInstance());
		mFragments.add(1, BoardFragment.newInstance());
		mFragments.add(2, GPUFragment.newInstance());
		mFragments.add(3, BatteryFragment.newInstance());
		mFragments.add(4, SensorsFragment.newInstance());
		mFragments.add(5, AboutFragment.newInstance());
	}

	@Override
	public Fragment getItem(int pos) {
		return mFragments.get(pos);
	}

	@Override
	public int getCount() {
		return mTitleList.size();
	}

	public CharSequence getPageTitle(int pos) {
		return mCtx.getResources().getString(mTitleList.get(pos));
	}

	public int getTitleID(int pos) {
		return mTitleList.get(pos);
	}

}
