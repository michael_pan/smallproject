package com.ypan.monitor;

import java.util.ArrayList;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.ypan.monitor.osutils.SensorListItem;
import com.ypan.monitor.osutils.Sensors;
import com.ypan.monitor.osutils.Sensors.SensorListener;

public class SensorActivity extends ActionBarActivity implements
		SensorListener {

	public static final String ITEMNAME = "item_name";
	private String name = "";
	private String[] sensorname;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setTitle(R.string.tab_title_sensors);
		Drawable bg = (Drawable) getResources().getDrawable(
				R.drawable.action_bar_bg);
		getSupportActionBar().setBackgroundDrawable(bg);
		setContentView(R.layout.list_item_sensors);
		if (getIntent() != null) {
			name= getIntent().getStringExtra(ITEMNAME);
		}
		Log.e("test", "name = "+name);
		sensorname = getResources().getStringArray(R.array.sensor);
		initview();
		Sensors.getInstance().setSensorListener(this);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
		default:
			break;
		}
		return true;
	}

	private void initview() {
		SensorListItem item = null;
		ArrayList<SensorListItem> list = Sensors.getInstance().getItems();
		for(SensorListItem it : list){
			if(it.name.equals(name)){
				item = it;
				break;
			}
		}
		if(item == null)
			return;
		TextView name = (TextView) findViewById(R.id.name_tv);
		TextView status = (TextView)findViewById(R.id.status_tv);
		TextView vendor = (TextView) findViewById(R.id.vendor_tv);
		TextView maxRange = (TextView) findViewById(R.id.maxRange_tv);
		TextView mindelay = (TextView) findViewById(R.id.minidelay_tv);
		TextView power = (TextView) findViewById(R.id.power_tv);
		TextView resolution = (TextView) findViewById(R.id.resolution_tv);
		TextView type = (TextView) findViewById(R.id.type_tv);
		TextView version = (TextView) findViewById(R.id.version_tv);
		TextView values = (TextView) findViewById(R.id.values_tv);

		name.setText("名称："+item.name);
		status.setText("状态："+item.status);
		vendor.setText("厂商: "+item.vendor);
		maxRange.setText("最大值: " + item.maxRange);
		mindelay.setText("最小延时：" + item.mindelay);
		power.setText("电流(mA): " + item.power);
		resolution.setText("分辨率：" + item.resolution);
		type.setText("类型：" + sensorname[item.type-1]);
		version.setText("版本：" + item.version);
		if (item.values != null)
			values.setText("V[x,y,z] = " + item.values[0] + "-"
					+ item.values[1] + "-" + item.values[2]);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Sensors.getInstance().onResume(this);

	}

	@Override
	protected void onPause() {
		super.onPause();
		Sensors.getInstance().onPause(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private long lasttime = 0;

	@Override
	public void onSensorChanged() {
		long curtime = System.currentTimeMillis();
		if (curtime - lasttime > 200) {
			initview();
			lasttime = curtime;
		}
	}

}
