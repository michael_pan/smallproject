package com.ypan.monitor.widget;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ypan.monitor.R;

/**
 * 自定义进度条,当发起网络请求时显示该对话框，让用户等待
 */
public class WaitingFragmentDlg extends DialogFragment {
	private int msgStrID = R.string.waiting;

	public static WaitingFragmentDlg newInstance(int msgStrID) {
		WaitingFragmentDlg f = new WaitingFragmentDlg();
		Bundle args = new Bundle();
		args.putInt("id", msgStrID);
		f.setArguments(args);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		msgStrID = getArguments().getInt("id");
		setStyle(DialogFragment.STYLE_NO_TITLE,
				android.R.style.Theme_Light_Panel);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.waiting_dialog, container, false);
		TextView tv = (TextView) v.findViewById(R.id.txt_custom_content);
		tv.setText(msgStrID);
		setCancelable(true);
		return v;
	}

}
