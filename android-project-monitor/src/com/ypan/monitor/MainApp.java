package com.ypan.monitor;

import android.app.Activity;
import android.app.Application;

import com.ypan.monitor.logger.SLog;
import com.ypan.monitor.utils.CrashHandler;

/**
 * @ClassName: MainApp
 * @Description: main application entry
 * @author Michael.Pan
 * @date 2012-6-18 下午02:52:49
 */
public class MainApp extends Application {

	public static MainApp sInstance = null;
	public static Activity sMainActivity = null;

	@Override
	public void onCreate() {
		super.onCreate();
		initCrashHandle();
		SLog.init(false, false, SLog.LEVEL_DEBUG, false);
		sInstance = this;
	}

	private void initCrashHandle() {
		CrashHandler cHandler = CrashHandler.getInstance();
		cHandler.init(getApplicationContext());
	}

}
