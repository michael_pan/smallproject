package com.ypan.monitor.views;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ypan.monitor.R;
import com.ypan.monitor.osutils.SensorListItem;
import com.ypan.monitor.osutils.Sensors;
import com.ypan.monitor.osutils.Sensors.SensorListener;

public class SensorsAdapter2 extends BaseAdapter implements SensorListener {

	private Context ctx;
	private ArrayList<SensorListItem> list;
	private String[] sensorname;
	public SensorsAdapter2(Context ctx) {
		this.ctx = ctx;
		this.list = new ArrayList<SensorListItem>();
		Sensors.getInstance().setSensorListener(this);
		sensorname = ctx.getResources().getStringArray(R.array.sensor);
		update();
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = LayoutInflater.from(ctx).inflate(R.layout.list_item4,
				null);
		TextView title = (TextView) convertView.findViewById(R.id.title_tv);
		TextView content = (TextView) convertView.findViewById(R.id.content_tv);

		SensorListItem item = list.get(position);
		
		if (item.type <= sensorname.length) {
			title.setText(sensorname[item.type-1]);
		}
		content.setText("支持" + "(" + item.name + ")");
		return convertView;
	}

	private void update() {
		list.clear();
		ArrayList<SensorListItem> sensors = Sensors.getInstance().getItems();
		for (SensorListItem item : sensors) {
			if (item.status.equals("Good")) {
				list.add(item);
			}
		}	
	}

	private long lasttime = 0;


	@Override
	public void onSensorChanged() {
		long curtime = System.currentTimeMillis();
		//if (curtime - lasttime > 5000) {
			update();
			notifyDataSetChanged();
			lasttime = curtime;
		//}
	}
}
