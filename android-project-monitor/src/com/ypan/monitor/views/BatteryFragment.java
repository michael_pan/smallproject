package com.ypan.monitor.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ypan.monitor.BaseFragment;
import com.ypan.monitor.R;

public class BatteryFragment extends BaseFragment {

	private BatteryAdapter batteryAdapter = null;

	public static BatteryFragment newInstance() {
		BatteryFragment fragment = new BatteryFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return initView(inflater, container, savedInstanceState);
	}

	private View initView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.listview, container, false);
		ListView lv = (ListView) view.findViewById(R.id.list);
		batteryAdapter = new BatteryAdapter(getActivity());
		lv.setAdapter(batteryAdapter);
		return view;
	}

	@Override
	public void onLoadData(boolean bforceUpdate) {
		if (batteryAdapter != null)
			batteryAdapter.update();
	}
}
