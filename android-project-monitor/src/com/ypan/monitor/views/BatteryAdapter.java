package com.ypan.monitor.views;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ypan.monitor.R;
import com.ypan.monitor.osutils.Battery;
import com.ypan.monitor.osutils.ListItem;

public class BatteryAdapter extends BaseAdapter {

	private Context ctx;
	private ArrayList<ListItem> list;

	public BatteryAdapter(Context ctx) {
		this.ctx = ctx;
		this.list = Battery.getInstance(ctx).getItems();
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = LayoutInflater.from(ctx).inflate(R.layout.list_item3,
				null);
		TextView titleTV = (TextView) convertView.findViewById(R.id.title_tv);
		TextView contentTV = (TextView) convertView
				.findViewById(R.id.content_tv);
		ListItem item = list.get(position);
		titleTV.setText(item.title);
		contentTV.setText(item.content);
		return convertView;
	}

	public void update() {
		this.list = Battery.getInstance(ctx).getItems();
		notifyDataSetChanged();
	}
}
