package com.ypan.monitor.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ypan.monitor.BaseFragment;
import com.ypan.monitor.R;

public class AboutFragment extends BaseFragment {

	private AboutAdapter adapter;

	public static AboutFragment newInstance() {
		AboutFragment fragment = new AboutFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return initView(inflater, container, savedInstanceState);
	}

	private View initView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.listview, container, false);
		ListView lv = (ListView) view.findViewById(R.id.list);
		adapter = new AboutAdapter(getActivity());
		lv.setAdapter(adapter);
		return view;
	}

	@Override
	public void onLoadData(boolean bforceUpdate) {

	}
}
