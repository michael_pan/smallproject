package com.ypan.monitor.views;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.ypan.monitor.BaseFragment;
import com.ypan.monitor.R;
import com.ypan.monitor.SensorActivity;
import com.ypan.monitor.osutils.SensorListItem;

public class SensorsFragment extends BaseFragment implements
		OnItemClickListener {

	private SensorsAdapter2 adapter;

	public static SensorsFragment newInstance() {
		SensorsFragment fragment = new SensorsFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return initView(inflater, container, savedInstanceState);
	}

	private View initView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.listview, container, false);
		ListView lv = (ListView) view.findViewById(R.id.list);
		adapter = new SensorsAdapter2(getActivity());
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
		return view;
	}

	@Override
	public void onLoadData(boolean bforceUpdate) {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(getActivity(), SensorActivity.class);
		SensorListItem item = (SensorListItem)adapter.getItem(position);
		intent.putExtra(SensorActivity.ITEMNAME, item.name);
		Log.d("test", "item.name = "+item.name);
		getActivity().startActivity(intent);
	}
}
