package com.ypan.monitor;

import android.R;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/**
 * @ClassName: BaseFragment
 * @Description: BaseFragment
 * @author Administrator
 * @date 2012-8-15 下午10:48:10
 */
public abstract class BaseFragment extends Fragment {
	public static final String TAG = "Monitor";

	// dialog id
	public static final int DIALOG_ABOUT = 1;

	// Menu id
	public static final int MENU_GAMES = 1; // 游戏
	public static final int MENU_FEEDBACK = 2; // 反馈

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	/**
	 * 显示对话框
	 * 
	 * @param dialog
	 * @param tag
	 */
	public void showDialog(FragmentManager fm, DialogFragment dialog, String tag) {
		FragmentTransaction st = fm.beginTransaction();
		Fragment tagDialog = fm.findFragmentByTag(tag);
		if (tagDialog != null) {
			st.remove(tagDialog);
		}
		st.add(dialog, tag);
		// 当activity onSaveInstanceState(outState) 方法执行之后仍然可以显示对话框
		st.commitAllowingStateLoss();
	}

	/**
	 * 隐藏对话框
	 * 
	 * @param tag
	 */
	public void dismissDialog(FragmentManager fm, String tag) {
		DialogFragment waitingDialog = (DialogFragment) fm
				.findFragmentByTag(tag);
		if (waitingDialog != null) {
			waitingDialog.dismissAllowingStateLoss();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
//		MenuItem item;
//		long time = System.currentTimeMillis();
//		if (time - 1390549401334L > 9 * DateUtils.DAY_IN_MILLIS) {
//			item = menu.add(0, MENU_GAMES, 0, R.string.games);
//			item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//		}
//		item = menu.add(0, MENU_GAMES, 0, R.string.games);
//		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//		item = menu.add(0, MENU_FEEDBACK, 0, R.string.feedback);
//		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_GAMES: {
//			UIUtils.gotoGames(getActivity());
		}
			break;
		case MENU_FEEDBACK: {
//			UIUtils.gotoFeedback(getActivity());
		}
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public abstract void onLoadData(boolean bforceUpdate);
}
