package com.ypan.monitor.osutils;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import com.ypan.monitor.R;

import android.app.Activity;
import android.app.ActivityManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;

public class MainBoard {
	private static MainBoard instance = null;

	private MainBoard() {

	}

	public static MainBoard getInstance() {
		synchronized (MainBoard.class) {
			if (instance == null) {
				instance = new MainBoard();
			}
			return instance;
		}
	}

	public ArrayList<ListItem> getItems(Activity ctx) {
		ArrayList<ListItem> list = new ArrayList<ListItem>();
		list.add(new ListItem(ctx.getString(R.string.model), getModel()));
		list.add(new ListItem(ctx.getString(R.string.manufacturer), getManufacturer()));
		list.add(new ListItem(ctx.getString(R.string.board), getBoard()));
		list.add(new ListItem(ctx.getString(R.string.buildid), getDisplay()));
		list.add(new ListItem(ctx.getString(R.string.hardware), getHardware()));
		list.add(new ListItem(ctx.getString(R.string.androidv), getAndroidVersion()));
		list.add(new ListItem(ctx.getString(R.string.kernelarch), getKernelArch()));
		list.add(new ListItem(ctx.getString(R.string.kernelver), getKernelVersion()));
		list.add(new ListItem(ctx.getString(R.string.screensize), getScreenResolution(ctx)));
		list.add(new ListItem(ctx.getString(R.string.screendpi), getScreenDPI(ctx) + " dpi"));
		list.add(new ListItem(ctx.getString(R.string.totalram), getTotalRAM() + " MB"));
		list.add(new ListItem(ctx.getString(R.string.avram), getAvailableRAM(ctx) + " MB"));
		list.add(new ListItem(ctx.getString(R.string.totalstorage), String
				.valueOf(getTotalMemory()) + " GB"));
		list.add(new ListItem(ctx.getString(R.string.avstorage), String
				.valueOf(getAvailableMemory()) + " GB"));
		list.add(new ListItem(ctx.getString(R.string.rootaccess), getRootAccess(ctx)));
		return list;
	}

	// Model 型号
	public static String getModel() {
		return Build.MODEL + " (" + Build.PRODUCT + ")";
	}

	// Manufacturer 制造商
	public static String getManufacturer() {
		return Build.MANUFACTURER;
	}

	// 主板名称
	public static String getBoard() {
		return Build.BOARD;
	}

	// Build ID
	public static String getDisplay() {
		return Build.DISPLAY;
	}

	// Hardware
	public static String getHardware() {
		return Build.HARDWARE;
	}

	// Android version
	public static String getAndroidVersion() {
		return Build.VERSION.RELEASE;
	}

	// Kernel Architecture
	public static String getKernelArch() {
		return System.getProperty("os.arch");
	}

	// 内核版本
	public static String getKernelVersion() {
		return "Linux " + System.getProperty("os.version") + " ("
				+ Build.VERSION.INCREMENTAL + ")";
	}

	// 屏幕宽高
	public static String getScreenResolution(Activity ctx) {
		DisplayMetrics metric = new DisplayMetrics();
		ctx.getWindowManager().getDefaultDisplay().getMetrics(metric);
		return metric.widthPixels + " x " + metric.heightPixels;
	}

	// 屏幕DPI
	public static int getScreenDPI(Activity ctx) {
		DisplayMetrics metric = new DisplayMetrics();
		ctx.getWindowManager().getDefaultDisplay().getMetrics(metric);
		return metric.densityDpi;
	}

	// Total RAM
	public static int getTotalRAM() {
		int i = 0;
		int j = 0;
		try {
			String[] arrayOfString = new RandomAccessFile("/proc/meminfo", "r")
					.readLine().split(" kB")[0].split(" ");
			i = Integer.parseInt(arrayOfString[(-1 + arrayOfString.length)]);
			j = Math.round(i / 1024);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return j;
	}

	// Available RAM
	public static int getAvailableRAM(Activity ctx) {
		ActivityManager mgr = (ActivityManager) ctx
				.getSystemService("activity");
		ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
		mgr.getMemoryInfo(memInfo);
		return (int) (memInfo.availMem / 1024L / 1024L);
	}

	// Total Memory
	public static float getTotalMemory() {
		StatFs localStatFs = new StatFs(Environment
				.getExternalStorageDirectory().getPath());
		return (float) localStatFs.getBlockSize()
				* (float) localStatFs.getBlockCount() / 1024.0F / 1024.0F
				/ 1024.0F;
	}

	// Available Memory
	public static float getAvailableMemory() {
		StatFs localStatFs = new StatFs(Environment
				.getExternalStorageDirectory().getPath());
		return (float) localStatFs.getBlockSize()
				* (float) localStatFs.getFreeBlocks() / 1024.0F / 1024.0F
				/ 1024.0F;
	}

	public static String getRootAccess(Activity ctx) {
		String root = ctx.getString(R.string.rootnot);
		if (isRooted(ctx)) {
			root = ctx.getString(R.string.rootyes);
		} else {
			root = ctx.getString(R.string.rootnot);
		}
		return root;
	}

	private static boolean isRooted(Activity ctx) {
		String str = Build.TAGS;
		if ((str != null) && (str.contains("test-keys")))
			return true;
		return new File("/bin/system/su").exists();
	}
}
