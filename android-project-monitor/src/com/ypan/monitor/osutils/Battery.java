package com.ypan.monitor.osutils;

import java.util.ArrayList;

import com.ypan.monitor.R;

import android.content.Context;
import android.os.BatteryManager;

public class Battery {

	private static Battery instance = null;
	private static Context mContext;
	public int health;
	public int level;
	public int scale;
	public int pluged;
	public int status;
	public int temp;
	public int voltage;
	public String technology;

	private Battery() {

	}

	public static Battery getInstance(Context ctx) {
		synchronized (Cpu.class) {
			if (instance == null) {
				instance = new Battery();
				mContext = ctx;
			}
			return instance;
		}
	}

	public ArrayList<ListItem> getItems() {
		ArrayList<ListItem> list = new ArrayList<ListItem>();
		list.add(new ListItem(mContext.getString(R.string.health), getHealth()));
		String level = String.valueOf(getLevel()) + " %";
		list.add(new ListItem(mContext.getString(R.string.level), level));
		list.add(new ListItem(mContext.getString(R.string.power), getPowerSource()));
		list.add(new ListItem(mContext.getString(R.string.status), getChargeStatus()));
		list.add(new ListItem(mContext.getString(R.string.temp), getTemp()));
		list.add(new ListItem(mContext.getString(R.string.voltage), getVol()));
		list.add(new ListItem(mContext.getString(R.string.tech), getTechnology()));
		return list;
	}

	// 电池电量
	public int getLevel() {
		if (scale == 0) {
			return 100;
		}
		return 100 * level / scale;
	}

	// 电池健康状态
	public String getHealth() {
		String szhealth = "Unknown";
		switch (health) {
		case BatteryManager.BATTERY_HEALTH_COLD:
			szhealth = "Cold";
			break;
		case BatteryManager.BATTERY_HEALTH_DEAD:
			szhealth = "Dead";
			break;
		case BatteryManager.BATTERY_HEALTH_GOOD:
			szhealth = "Good";
			break;
		case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
			szhealth = "Over Voltage";
			break;
		case BatteryManager.BATTERY_HEALTH_OVERHEAT:
			szhealth = "Overheat";
			break;
		case BatteryManager.BATTERY_HEALTH_UNKNOWN:
			szhealth = "Unknown";
			break;
		case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
			szhealth = "Unspecified failure";
		default:
			break;
		}
		return szhealth;
	}

	// 电力来源
	public String getPowerSource() {
		String source = "Unknown";
		switch (pluged) {
		case 0:
			source = "Battery";
			break;
		case BatteryManager.BATTERY_PLUGGED_AC:
			source = "AC Charger";
			break;
		case BatteryManager.BATTERY_PLUGGED_USB:
			source = "USB Port";
			break;
		case BatteryManager.BATTERY_PLUGGED_WIRELESS:
			source = "Wireless";
			break;
		default:
			break;
		}
		return source;
	}

	// 获取电池充电状态
	public String getChargeStatus() {
		String charge = "Unknown";
		switch (status) {
		case BatteryManager.BATTERY_STATUS_CHARGING:
			charge = "Charging";
			break;
		case BatteryManager.BATTERY_STATUS_DISCHARGING:
			charge = "Discharging";
			break;
		case BatteryManager.BATTERY_STATUS_FULL:
			charge = "Full";
			break;
		case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
			charge = "Not charging";
			break;
		default:
			break;
		}
		return charge;
	}

	// 获取温度
	public String getTemp() {
		float ftemp = Float.valueOf(temp / 10.0f);
		return String.format("%.1f", ftemp) + " °C";
	}

	// 获取电压
	public String getVol() {
		return "" + voltage + " mV";
	}
	
	//获取电池类型
	public String getTechnology(){
		return technology;
	}

}
