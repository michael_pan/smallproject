package com.ypan.monitor.osutils;

import java.util.ArrayList;

import com.ypan.monitor.R;

import android.content.Context;

public class GPU {
	private static GPU instance = null;
	private static Context context = null;
	public String verdor;
	public String renderer;

	private GPU() {

	}

	public static GPU getInstance(Context ctx) {
		synchronized (GPU.class) {
			if (instance == null) {
				instance = new GPU();
				context = ctx;
			}
			return instance;
		}
	}

	public String getVendor() {
		return verdor;
	}

	public String getRender() {
		return renderer;
	}

	public ArrayList<ListItem> getItems() {
		ArrayList<ListItem> list = new ArrayList<ListItem>();
		list.add(new ListItem(context.getString(R.string.vendor), getVendor()));
		list.add(new ListItem(context.getString(R.string.render), getRender()));
		return list;
	}
}
