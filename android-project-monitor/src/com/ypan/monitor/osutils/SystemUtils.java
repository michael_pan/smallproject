package com.ypan.monitor.osutils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class SystemUtils {
	public static String get_cpu_info() {
		return null;
	}

	// 读取输入流，将它转为字符串
	public static String readFully(InputStream inputStream) throws IOException {
		StringBuilder sb = new StringBuilder();
		Scanner lScanner = new Scanner(inputStream);
		while (lScanner.hasNextLine()) {
			sb.append(lScanner.nextLine());
		}
		return sb.toString();
	}

	public static int readSystemFileAsInt(String path) throws Exception {
		try {
			int i = Integer.parseInt(readFully(new ProcessBuilder(new String[] {
					"/system/bin/cat", path }).start().getInputStream()));
			return i;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
