package com.ypan.monitor.osutils;

public class ListItem {
	public String title;
	public String content;

	public ListItem(String title, String content) {
		this.title = title;
		this.content = content;
	}
}
