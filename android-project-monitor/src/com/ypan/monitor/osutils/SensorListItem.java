package com.ypan.monitor.osutils;

import android.hardware.Sensor;

public class SensorListItem {
	public String name;
	public String vendor;
	public float maxRange;
	public int mindelay; // ms
	public float power; // float mA
	public float resolution; // float
	public int type;
	public int version;
	public float[] values = new float[3];
	public String status;

	public SensorListItem(Sensor ss) {
		name = ss.getName();
		vendor = ss.getVendor();
		maxRange = ss.getMaximumRange();
		mindelay = ss.getMinDelay();
		power = ss.getPower();
		resolution = ss.getResolution();
		type = ss.getType();
		version = ss.getVersion();
		status = "Unknown";
	}
}
