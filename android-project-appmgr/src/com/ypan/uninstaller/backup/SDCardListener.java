
package com.ypan.uninstaller.backup;

import android.os.FileObserver;

import com.ypan.uninstaller.logger.SLog;

/**
 * @ClassName: SDCardListener
 * @Description: sd卡上的目录创建监听器
 * @author Administrator
 * @date 2012-8-30 上午12:23:29
 */
public class SDCardListener extends FileObserver {
    private static final String TAG = SDCardListener.class.getSimpleName();
    private onFileChangedListener mlistener = null;

    public SDCardListener(onFileChangedListener listener, String path) {
        super(path);
        mlistener = listener;
    }

    @Override
    public void onEvent(int event, String path) {
        switch (event) {
            case FileObserver.CLOSE_WRITE:
                mlistener.onChange(event);
                SLog.d(TAG, "CLOSE_WRITE" + ",path:" + path);
                break;
            case FileObserver.DELETE:
                mlistener.onChange(event);
                SLog.d(TAG, "DELETE" + ",path:" + path);
                break;
            default:
                break;
        }
    }
}
