
package com.ypan.uninstaller.backup;

/**
 * @ClassName: FileChanged
 * @Description: 监控文件夹变化
 * @author Administrator
 * @date 2012-8-30 上午12:33:23
 */
public interface onFileChangedListener {
    public void onChange(int msg);
}
