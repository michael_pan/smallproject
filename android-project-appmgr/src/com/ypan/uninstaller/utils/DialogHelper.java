package com.ypan.uninstaller.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cl.partner.AdActivity;
import com.cl.partner.FeedbackActivity;
import com.ypan.uninstaller.MainApp;
import com.ypan.uninstaller.R;
import com.ypan.uninstaller.widget.WaitingFragmentDlg;

public class DialogHelper {

	public static void Toast(int resId, boolean isLong) {
		int duration = isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(MainApp.sMainActivity, resId, duration);
		toast.setGravity(Gravity.BOTTOM, 0, 50);
		toast.show();
	}

	// about dialog
	public static class AboutDlgFragment extends DialogFragment {

		public static AboutDlgFragment newInstance() {
			AboutDlgFragment frag = new AboutDlgFragment();
			Bundle args = new Bundle();
			frag.setArguments(args);
			return frag;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					MainApp.sMainActivity);
			builder.setTitle(R.string.title_about);
			View view = LayoutInflater.from(MainApp.sMainActivity).inflate(
					R.layout.dialog_about, null);
			TextView version = (TextView) view.findViewById(R.id.version);
			version.append(" ");
			version.append(MainApp.sMainActivity.getResources().getString(
					R.string.app_name));
			version.append(" ");
			version.append(getVersion());
			TextView email = (TextView) view.findViewById(R.id.email);
			email.append("panyingyun" + "@" + "gmail" + ".com");
			builder.setView(view);
			builder.setNeutralButton(MainApp.sMainActivity.getResources()
					.getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							return;
						}
					});
			return builder.create();
		}
	}

	public static void showAboutDialog(FragmentManager manager) {
		//AboutDlgFragment.newInstance().show(manager, "aboutdlg");
		Intent intent = new Intent(MainApp.sMainActivity, AdActivity.class);
		MainApp.sMainActivity.startActivity(intent);
	}
	
	
	public static void showSearchDialog(FragmentManager manager) {
		//AboutDlgFragment.newInstance().show(manager, "aboutdlg");
		Intent intent = new Intent(MainApp.sMainActivity, FeedbackActivity.class);
		MainApp.sMainActivity.startActivity(intent);
	}

	// show sort dialog
	public static class SortDlgFragment extends DialogFragment {
		SortListener sortlistener;

		public static SortDlgFragment newInstance(SortListener sortlistener) {
			SortDlgFragment frag = new SortDlgFragment();
			Bundle args = new Bundle();
			frag.setArguments(args);
			frag.setListener(sortlistener);
			return frag;
		}

		public void setListener(SortListener sortlistener) {
			this.sortlistener = sortlistener;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					MainApp.sMainActivity);
			builder.setTitle(R.string.title_sort);
			View view = LayoutInflater.from(MainApp.sMainActivity).inflate(
					R.layout.dialog_sort, null);
			builder.setView(view);
			final RadioButton timeDescBtn = (RadioButton) view
					.findViewById(R.id.sort_time_desc);
			final RadioButton timeAscBtn = (RadioButton) view
					.findViewById(R.id.sort_time_asc);
			final RadioButton nameDescBtn = (RadioButton) view
					.findViewById(R.id.sort_name_desc);
			final RadioButton nameAscBtn = (RadioButton) view
					.findViewById(R.id.sort_name_asc);
			int sortType = SharePreferences.getSortType();
			if (Define.SORT_BY_TIME_DESC == sortType) {
				timeDescBtn.setChecked(true);
			} else if (Define.SORT_BY_TIME_ASC == sortType) {
				timeAscBtn.setChecked(true);
			} else if (Define.SORT_BY_NAME_DESC == sortType) {
				nameDescBtn.setChecked(true);
			} else if (Define.SORT_BY_NAME_ASC == sortType) {
				nameAscBtn.setChecked(true);
			}
			DialogInterface.OnClickListener listener = new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (sortlistener == null)
						return;
					if (timeDescBtn.isChecked()) {
						sortlistener.onSort(Define.SORT_BY_TIME_DESC);
					} else if (timeAscBtn.isChecked()) {
						sortlistener.onSort(Define.SORT_BY_TIME_ASC);
					} else if (nameDescBtn.isChecked()) {
						sortlistener.onSort(Define.SORT_BY_NAME_DESC);
					} else if (nameAscBtn.isChecked()) {
						sortlistener.onSort(Define.SORT_BY_NAME_ASC);
					}
				}
			};
			builder.setNeutralButton(
					MainApp.sMainActivity.getString(R.string.ok), listener);
			return builder.create();
		}
	}

	public interface SortListener {
		public void onSort(int sortType);
	}

	public static void showSortDialog(FragmentManager manager,
			SortListener sortlistener) {
		SortDlgFragment.newInstance(sortlistener).show(manager, "sortdlg");
	}

	// 显示备份对话框
	public static class BackupDlgFragment extends DialogFragment {
		private String file;
		private long size;
		private String name;
		private BackupListener backListener;

		public static BackupDlgFragment newInstance(String file, String name,
				long size, BackupListener backListener) {
			BackupDlgFragment frag = new BackupDlgFragment();
			Bundle args = new Bundle();
			frag.setArguments(args);
			frag.setFile(file);
			frag.setName(name);
			frag.setSize(size);
			frag.setBackupListener(backListener);
			return frag;
		}

		public void setFile(String file) {
			this.file = file;
		}

		public void setSize(long size) {
			this.size = size;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setBackupListener(BackupListener listener) {
			this.backListener = listener;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					MainApp.sMainActivity);
			builder.setTitle(R.string.title_backup);
			View view = LayoutInflater.from(MainApp.sMainActivity).inflate(
					R.layout.dialog_backup, null);
			builder.setView(view);
			final TextView pathTV = (TextView) view.findViewById(R.id.tv_path);
			pathTV.append(SharePreferences.getBackupPath());
			final TextView nameTV = (TextView) view.findViewById(R.id.tv_name);
			nameTV.append(name + ".apk");
			DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (backListener == null)
						return;
					backListener.onBackup(file, name, size);
				}
			};
			builder.setNeutralButton(MainApp.sMainActivity.getResources()
					.getString(R.string.ok), listener);
			return builder.create();
		}
	}

	public interface BackupListener {
		public void onBackup(String file, String name, long size);
	}

	public static BackupDlgFragment createBackupDialog(String file,
			String name, long size, BackupListener backListener) {
		return BackupDlgFragment.newInstance(file, name, size, backListener);
	}

	private static String getVersion() {
		try {
			return MainApp.sMainActivity.getPackageManager().getPackageInfo(
					MainApp.sMainActivity.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			return "version not found";
		}
	}

	// 显示等待对话框
	public static DialogFragment createWaitingDialog(int strID) {
		return WaitingFragmentDlg.newInstance(strID);
	}

	// 显示选择对话框
	public static class OptionDlgFragment extends DialogFragment {
		// 选择监听器
		private OptionListener optionListener = null;
		private int pos = 0;

		public static OptionDlgFragment newInstance(int position,
				OptionListener listener) {
			OptionDlgFragment frag = new OptionDlgFragment();
			Bundle args = new Bundle();
			frag.setArguments(args);
			frag.setOptionListener(listener);
			frag.setPos(position);
			return frag;
		}

		public void setPos(int position) {
			this.pos = position;
		}

		public void setOptionListener(OptionListener listener) {
			this.optionListener = listener;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					MainApp.sMainActivity);
			DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (optionListener != null) {
						// 1 运行 2查看详情 3 备份
						optionListener.Option(which, pos);
					}
				}
			};
			builder.setItems(R.array.options, listener);
			return builder.create();
		}
	}

	public interface OptionListener {
		public void Option(int id, int position);
	}

	public static OptionDlgFragment createOptionDialog(int position,
			OptionListener listener) {
		return OptionDlgFragment.newInstance(position, listener);
	}
}
