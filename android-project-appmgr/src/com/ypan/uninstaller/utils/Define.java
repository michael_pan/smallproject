
package com.ypan.uninstaller.utils;

import android.os.Environment;

/**
 * @ClassName: Constants
 * @Description: Constants Define here.
 * @author Michael.Pan
 * @date 2012-6-18 下午02:18:07
 */
public class Define {

    /* application cache's directory */
    public static final String UNINSTALL = Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/.uninstall/";

    /** application normal log directory **/
    public static final String NORMAL_LOG_PATH = UNINSTALL + "log/";
    /** application crash log directory **/
    public static final String CRASH_LOG_PATH = UNINSTALL + "crash/";
    /** application icon file cache directory **/
    public static final String ICON_CACHE_PATH = UNINSTALL + "cache/";
    /** application default backup apk dir **/
    public static final String BACKUP_APK_PATH = Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/backup/";

    public static final int SORT_BY_TIME_DESC = 1;
    public static final int SORT_BY_TIME_ASC = 2;
    public static final int SORT_BY_NAME_DESC = 3;
    public static final int SORT_BY_NAME_ASC = 4;
}
