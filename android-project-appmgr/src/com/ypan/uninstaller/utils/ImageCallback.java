
package com.ypan.uninstaller.utils;

import android.graphics.Bitmap;

import com.ypan.uninstaller.adapter.ApkInfo;

/**
 * @ClassName: ImageCallback
 * @Description: ImageCallback
 * @author Administrator
 * @date 2012-9-3 下午11:46:20
 */
public interface ImageCallback {
    public void onLoadImage(Bitmap bm, String photoID, ApkInfo info);
}
