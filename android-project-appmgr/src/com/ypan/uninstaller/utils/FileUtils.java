
package com.ypan.uninstaller.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.ypan.uninstaller.all.ProcessCallback;
import com.ypan.uninstaller.logger.SLog;

/**
 * @ClassName: FileUtils
 * @Description:文件拷贝
 * @author Michael.Pan
 * @date 2012-6-19 下午01:39:29
 */
public class FileUtils {

	private static final int EACH_BLOCK = 10;
    private static final int BUFFER_SIZE = EACH_BLOCK*1024; // 10 KB
    private static final byte[] mBytes = new byte[BUFFER_SIZE];

    /**
     * CopyFile from srcpath to dstpath
     * 
     * @param srcFile
     * @param dstFile
     */
    public static void CopyFile(String srcFile, String dstFile, ProcessCallback callback) {
        FileInputStream in = null;
        FileOutputStream out = null;
        if(srcFile == null || dstFile == null)
        	return;
        try {
            in = new FileInputStream(srcFile);
            out = new FileOutputStream(dstFile);
            copyStream(in, out, callback);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void copyStream(InputStream is, OutputStream os, ProcessCallback callback)
            throws IOException {

        int process = 0; // 0K
        int count = 0;
        while ((count = is.read(mBytes)) > 0) {
            os.write(mBytes, 0, count);
            process += EACH_BLOCK;
            if (callback != null) {
                callback.onProcess(process);
            }
        }
    }

    /**
     * @param file
     * @param zipfile
     */
    public static void zipFile(String file, String zipfile) {
        try {
            FileInputStream in = new FileInputStream(file);
            FileOutputStream out = new FileOutputStream(zipfile);
            ZipOutputStream zipOut = new ZipOutputStream(out);
            ZipEntry entry = new ZipEntry(file);
            zipOut.putNextEntry(entry);
            int nNumber;
            byte[] buffer = new byte[512];
            while ((nNumber = in.read(buffer)) != -1)
                zipOut.write(buffer, 0, nNumber);
            zipOut.close();

            out.close();
            in.close();

        } catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * @param sPath
     * @return
     */
    public static void deleteFile(String sPath) {
        File file = new File(sPath);
        if (file.isFile() && file.exists()) {
            file.delete();
        }
    }

    public static boolean Bitmap2JPEG(Bitmap bmp, String filepath) {
        if (bmp == null || filepath == null)
            return false;

        OutputStream stream = null;
        try {
            File file = new File(filepath);
            File dir = new File(file.getParent());
            if (!dir.exists())
                dir.mkdirs();
            if (file.exists())
                file.delete();

            stream = new FileOutputStream(filepath);

            if (bmp.compress(Bitmap.CompressFormat.PNG, 85, stream)) {
                stream.flush();
                stream.close();
                return true;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public static Bitmap JpegToBitmap(String path) {
        File file = new File(path);
        if (!file.exists()) {
            SLog.w("FileUtils", "icon cache file is not exits");
            return null;
        }
        Bitmap bm = null;
        BitmapFactory.Options bfoOptions = new BitmapFactory.Options();
        bfoOptions.inDither = false;
        bfoOptions.inPurgeable = true;
        bfoOptions.inInputShareable = true;
        bfoOptions.inTempStorage = new byte[32 * 1024];

        FileInputStream fs = null;
        try {
            fs = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            SLog.d("FileUtils", "icon cache file is not exits");
        }

        try {
            if (fs != null)
                bm = BitmapFactory.decodeFileDescriptor(fs.getFD(), null, bfoOptions);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fs != null) {
                try {
                    fs.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }

        return bm;
    }

}
