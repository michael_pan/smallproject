package com.ypan.uninstaller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.cl.clservice.CL;
import com.viewpagerindicator.TabPageIndicator;
import com.ypan.uninstaller.logger.SLog;

/**
 * @ClassName: MainActivity
 * @Description: 主Activity
 * @author Administrator
 * @date 2012-8-7 下午10:25:09
 */
public class MainActivity extends ActionBarActivity implements
		OnPageChangeListener, OnClickListener {

	private static final String TAG = "Uninstall";
	TabPageIndicator mTabIndicator;
	ViewPager mViewPager;
	View mBottonBar;
	TextView mAboutTV;
	TextView mUninstallTV;
	TextView mSelAllTV;
	UninstallFragmentAdapter mFragAdapter;
	BaseFragment mCurFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.main_tab);
		initView(savedInstanceState);
		CL.enableCLService(this, "");
		registerAppReceiver();
	}

	@Override
	protected void onDestroy() {
		unregisterAppReceiver();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		MainApp.sMainActivity = this;
		// 初始化数据
		mCurFragment = (BaseFragment) mFragAdapter.getItem(0);
		mCurFragment.onLoadData(true);
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	private void initView(Bundle savedInstanceState) {
		// init views
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mFragAdapter = new UninstallFragmentAdapter(this,
				getSupportFragmentManager());
		mViewPager.setAdapter(mFragAdapter);
		mViewPager.setOffscreenPageLimit(3);
		mTabIndicator = (TabPageIndicator) findViewById(R.id.titles);
		mTabIndicator.setViewPager(mViewPager);
		mTabIndicator.setOnPageChangeListener(this);
		mBottonBar = (View) findViewById(R.id.bottom_bar);
		mAboutTV = (TextView) findViewById(R.id.settings_tv);
		mAboutTV.setOnClickListener(this);
		mUninstallTV = (TextView) findViewById(R.id.uninstall_tv);
		mUninstallTV.setOnClickListener(this);
		mSelAllTV = (TextView) findViewById(R.id.select_all_tv);
		mSelAllTV.setOnClickListener(this);
		// set actionbar background
		Drawable bg = (Drawable) getResources().getDrawable(
				R.drawable.action_bar_bg);
		getSupportActionBar().setBackgroundDrawable(bg);
	}

	@Override
	public void onPageScrollStateChanged(int pos) {

	}

	@Override
	public void onPageScrolled(int pos, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int pos) {
		if (mFragAdapter.getTitleID(pos) == R.string.tab_backup) {
			mAboutTV.setText(R.string.delete);
			mUninstallTV.setText(R.string.install);
			mSelAllTV.setText(R.string.select_all);
		} else {
			mAboutTV.setText(R.string.pref_settings);
			mUninstallTV.setText(R.string.uninstall);
			mSelAllTV.setText(R.string.select_all);
		}

		BaseFragment curFragment = (BaseFragment) mFragAdapter.getItem(pos);
		curFragment.onLoadData(false);
		// Log.e("test", "onPageSelected pos = "+pos);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		int pos = mViewPager.getCurrentItem();
		BaseFragment f = (BaseFragment) mFragAdapter.getItem(pos);
		switch (id) {
		case R.id.settings_tv:
			f.onDeleteClick();
			break;
		case R.id.uninstall_tv: {
			f.onUninstallClick();
		}
			break;
		case R.id.select_all_tv: {
			if (mSelAllTV.getText() == getResources().getString(
					R.string.unselect_all)) {
				f.onUnSelAllClick();
				mSelAllTV.setText(R.string.select_all);
			} else {
				f.onSelAllClick();
				mSelAllTV.setText(R.string.unselect_all);
			}
		}
			break;
		default:
			break;
		}
	}

	public class ApplicationChangeReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			if (Intent.ACTION_PACKAGE_ADDED.equals(intent.getAction())) {
				String packageName = intent.getDataString().substring(8);
				if (mCurFragment != null)
					mCurFragment.onLoadData(true);
				SLog.e("test", "install packageName = " + packageName);
			} else if (Intent.ACTION_PACKAGE_REMOVED.equals(intent.getAction())) {
				String packageName = intent.getDataString().substring(8);
				if (mCurFragment != null)
					mCurFragment.onLoadData(true);
				SLog.e("test", "uninstall packageName = " + packageName);
			}
		}
	}

	// Register application change receiver
	private ApplicationChangeReceiver mReceiver = null;

	private void registerAppReceiver() {
		mReceiver = new ApplicationChangeReceiver();
		RegisterSystemEventReceiver(mReceiver);
	}

	private void unregisterAppReceiver() {
		if (null != mReceiver) {
			unregisterReceiver(mReceiver);
			mReceiver = null;
		}
	}

	private void RegisterSystemEventReceiver(BroadcastReceiver receiver) {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
		intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		intentFilter.addDataScheme("package");
		registerReceiver(receiver, intentFilter);
	}

}
