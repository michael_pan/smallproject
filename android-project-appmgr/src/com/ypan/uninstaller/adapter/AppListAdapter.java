﻿package com.ypan.uninstaller.adapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.ypan.uninstaller.R;
import com.ypan.uninstaller.utils.IconCache;
import com.ypan.uninstaller.utils.MD5Utils;

/**
 * @ClassName: AppListAdapter
 * @Description: 应用列表Adapter
 * @author Michael.Pan
 * @date 2012-6-18 下午02:12:47
 */

public class AppListAdapter extends BaseAdapter {
	private static final String TAG = "AppListAdapter";
	private final LayoutInflater mInflater;
	private ArrayList<ApkInfo> mApkList;
	private boolean mHideVersion = false;

	public AppListAdapter(LayoutInflater inflater, ArrayList<ApkInfo> apkList) {
		// Cache the LayoutInflate to avoid asking for a new one each time.
		mInflater = inflater;
		mApkList = apkList;
		mHideVersion = false;
	}

	public AppListAdapter(LayoutInflater inflater, ArrayList<ApkInfo> apkList,
			Boolean hideVersion) {
		// Cache the LayoutInflate to avoid asking for a new one each time.
		mInflater = inflater;
		mApkList = apkList;
		mHideVersion = hideVersion;
	}

	public void update(ArrayList<ApkInfo> apkList) {
		mApkList = apkList;
		notifyDataSetChanged();
	}

	public void checkPos(int pos) {
		if (pos < 0 || pos >= mApkList.size()) {
			return;
		}
		ApkInfo info = mApkList.get(pos);
		if (info != null) {
			info.isChecked = !info.isChecked;
		}
		notifyDataSetChanged();
	}

	public void checkAll() {
		int size = mApkList.size();
		for (int i = 0; i < size; i++) {
			ApkInfo info = mApkList.get(i);
			if (info != null)
				info.isChecked = true;
		}
		notifyDataSetChanged();
	}

	public void uncheckAll() {
		int size = mApkList.size();
		for (int i = 0; i < size; i++) {
			ApkInfo info = mApkList.get(i);
			if (info != null)
				info.isChecked = false;
		}
		notifyDataSetChanged();
	}

	public ArrayList<ApkInfo> getApkList() {
		return mApkList;
	}

	public ArrayList<ApkInfo> getCheckedList() {
		ArrayList<ApkInfo> list = new ArrayList<ApkInfo>();
		int size = mApkList.size();
		for (int i = 0; i < size; i++) {
			ApkInfo info = mApkList.get(i);
			if (info != null && info.isChecked)
				list.add(info);
		}
		return list;
	}

	@Override
	public int getCount() {
		return mApkList.size();
	}

	@Override
	public Object getItem(int position) {
		return mApkList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item, null);
			holder = new ViewHolder();
			holder.TVappname = (TextView) convertView
					.findViewById(R.id.appname);
			holder.TVsize = (TextView) convertView.findViewById(R.id.size);
			holder.TVtime = (TextView) convertView.findViewById(R.id.time);
			holder.TVinfo = (TextView) convertView
					.findViewById(R.id.information);
			holder.icon = (ImageView) convertView.findViewById(R.id.icon);
			holder.box = (CheckBox) convertView.findViewById(R.id.checkbox);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		ApkInfo info = mApkList.get(position);
		if (info == null) {
			return convertView;
		}
		// set app name
		if (info.version == null || mHideVersion) {
			holder.TVappname.setText(info.appName);
		} else {
			holder.TVappname.setText(info.appName + " " + info.version);
		}
		// set time
		holder.TVtime.setText(new Date(info.modifiedTime).toLocaleString());
		// set size
		long size = info.apkSize;
		DecimalFormat df = new DecimalFormat("0.00");
		if (size < 1000 * 1024) {
			float temp = (float) (size / 1024.0);
			holder.TVsize.setText(df.format(temp) + "KB");
		} else {
			float temp = (float) (size / (1024.0 * 1024.0));
			holder.TVsize.setText(df.format(temp) + "MB");
		}
		holder.TVinfo.setText("");

		holder.box.setChecked(info.isChecked);

		String temp = info.appName + info.version + info.modifiedTime;
		IconCache.getInstance().getIcon(MD5Utils.toMd5(temp.getBytes()),
				holder.icon, info);
		return convertView;
	}

	static class ViewHolder {
		TextView TVappname;
		TextView TVtime;
		TextView TVsize;
		TextView TVinfo;
		ImageView icon;
		CheckBox box;
	}
}
