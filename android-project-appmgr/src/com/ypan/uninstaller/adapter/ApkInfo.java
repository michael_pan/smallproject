﻿package com.ypan.uninstaller.adapter;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.text.TextUtils;

import java.io.File;
import java.io.Serializable;

/**
 * @ClassName: ApkInfo
 * @Description: ApkInfo
 * @author Michael.Pan
 * @date 2012-6-18 下午02:12:38
 */

public class ApkInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	public String packageName;
	public String appName;
	public String version;
	public int versionCode;
	public String dataDir;
	public long modifiedTime;
	public long apkSize;
	public String backpath;
	public boolean isChecked;

	public ApkInfo(String packageName, String appName, Bitmap icon,
			String version, int versionCode, String dataDir, long modifiedTime,
			long apkSize) {
		this.packageName = packageName;
		this.appName = appName;
		this.version = TextUtils.isEmpty(version) ? "" : version;
		this.versionCode = versionCode;
		this.dataDir = dataDir;
		this.modifiedTime = modifiedTime;
		this.apkSize = apkSize;
		this.backpath = null;
		isChecked = false;
	}

	public ApkInfo(PackageManager pkManager, PackageInfo pkinfo) {
		this.packageName = pkinfo.packageName;
		this.appName = pkManager.getApplicationLabel(pkinfo.applicationInfo)
				.toString();
		this.version = TextUtils.isEmpty(pkinfo.versionName) ? ""
				: pkinfo.versionName;
		this.versionCode = pkinfo.versionCode;
		this.dataDir = pkinfo.applicationInfo.publicSourceDir;
		File apkfile = new File(pkinfo.applicationInfo.publicSourceDir);
		this.apkSize = apkfile.length();
		this.modifiedTime = apkfile.lastModified();
		this.backpath = null;
		isChecked = false;
	}

	public ApkInfo(PackageManager pkManager, PackageInfo pkinfo, String apkpath) {
		this.packageName = pkinfo.packageName;
		String path = apkpath.trim();
		this.appName = path.substring(path.lastIndexOf("/") + 1).replaceAll(
				".apk", "");
		this.version = TextUtils.isEmpty(pkinfo.versionName) ? ""
				: pkinfo.versionName;
		this.versionCode = pkinfo.versionCode;
		this.dataDir = pkinfo.applicationInfo.publicSourceDir;
		File apkfile = new File(path);
		this.apkSize = apkfile.length();
		this.modifiedTime = apkfile.lastModified();
		this.backpath = path;
		isChecked = false;
	}

}
