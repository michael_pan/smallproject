
package com.ypan.uninstaller;

import android.app.Activity;
import android.app.Application;

import com.ypan.uninstaller.logger.SLog;
import com.ypan.uninstaller.utils.CrashHandler;

/**
 * @ClassName: MainApp
 * @Description: main application entry
 * @author Michael.Pan
 * @date 2012-6-18 下午02:52:49
 */
public class MainApp extends Application {

    public static MainApp sInstance = null;
    public static Activity sMainActivity = null;
    public static boolean isLoadedApp = false;
    public static boolean isLoadedBackup = false;
    @Override
    public void onCreate() {
        super.onCreate();
        initCrashHandle();
        SLog.init(false, false, SLog.LEVEL_DEBUG, false);
        sInstance = this;
    }

    private void initCrashHandle() {
        CrashHandler cHandler = CrashHandler.getInstance();
        cHandler.init(getApplicationContext());
    }
    
    
}
