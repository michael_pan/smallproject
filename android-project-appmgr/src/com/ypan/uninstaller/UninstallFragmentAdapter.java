package com.ypan.uninstaller;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ypan.uninstaller.all.UninstallFragment;
import com.ypan.uninstaller.backup.BackupFragment;

public class UninstallFragmentAdapter extends FragmentPagerAdapter {
	private ArrayList<Integer> mTitleList;
	private Context mCtx;
	private ArrayList<BaseFragment> mFragments = new ArrayList<BaseFragment>();

	public UninstallFragmentAdapter(Context ctx, FragmentManager fm) {
		super(fm);
		mCtx = ctx;
		mTitleList = new ArrayList<Integer>();
		mTitleList.add(0, R.string.tab_installed_user);
		mTitleList.add(1, R.string.tab_installed_system);
		mTitleList.add(2, R.string.tab_backup);
		mFragments.add(0,UninstallFragment.newInstance(false));
		mFragments.add(1,UninstallFragment.newInstance(true));
		mFragments.add(2,BackupFragment.newInstance());
	}

	@Override
	public Fragment getItem(int pos) {
		return mFragments.get(pos);
	}

	@Override
	public int getCount() {
		return mTitleList.size();
	}

	public CharSequence getPageTitle(int pos) {
		return mCtx.getResources().getString(mTitleList.get(pos));
	}

	public int getTitleID(int pos) {
		return mTitleList.get(pos);
	}

	
}
