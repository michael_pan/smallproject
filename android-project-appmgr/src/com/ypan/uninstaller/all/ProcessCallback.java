
package com.ypan.uninstaller.all;

/**
 * @ClassName: ProcessCallback
 * @Description: Process Callback
 * @author Michael.Pan
 * @date 2012-8-9 下午07:06:33
 */
public interface ProcessCallback {
    public void onProcess(int size);
}
