package com.ypan.uninstaller;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

/**
 * @ClassName: BaseFragment
 * @Description: BaseFragment
 * @author Administrator
 * @date 2012-8-15 下午10:48:10
 */
public abstract class BaseFragment extends Fragment {
	// Options Menu's IDs
	public static final int MENU_OPT = Menu.FIRST;
	public static final int MENU_ABOUT = MENU_OPT + 1; // 关于（查看版本信息）
	public static final int MENU_SORT = MENU_OPT + 2; // 排序
	public static final int MENU_SEARCH = MENU_OPT + 3; // 搜索

	// Context Menu's IDs
	public static final int MENU_CTX = Menu.FIRST + 100;
	public static final int MENU_SHARE = MENU_CTX + 1; // 清空所有会话
	public static final int MENU_BACKUP = MENU_CTX + 2; // 备份
	public static final int MENU_UNINSTALL = MENU_CTX + 3;// 卸载
	public static final int MENU_INSTALL = MENU_CTX + 4; // 安装
	public static final int MENU_DELETE = MENU_CTX + 5; // 删除
	public static final int MENU_START = MENU_CTX + 6; // 启动运行App
	public static final int MENU_START_DETAIL = MENU_CTX + 7; // 启动运行APP 详情
	public static final int MENU_START_MARKET = MENU_CTX + 8; // 在应用市场中查看
	// Context menu groupid
	public static final int MENU_GROUPID_UNINSTALL = 1;
	public static final int MENU_GROUPID_BACKUP = 2;

	// dialog id
	public static final int DIALOG_ABOUT = 1;
	public static final int DIALOG_SORT = 2;
	public static final int DIALOG_SEARCH = 3;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	/**
	 * 显示对话框
	 * 
	 * @param dialog
	 * @param tag
	 */
	public void showDialog(FragmentManager fm, DialogFragment dialog, String tag) {
		FragmentTransaction st = fm.beginTransaction();
		Fragment tagDialog = fm.findFragmentByTag(tag);
		if (tagDialog != null) {
			st.remove(tagDialog);
		}
		st.add(dialog, tag);
		// 当activity onSaveInstanceState(outState) 方法执行之后仍然可以显示对话框
		st.commitAllowingStateLoss();
	}

	/**
	 * 隐藏对话框
	 * 
	 * @param tag
	 */
	public void dismissDialog(FragmentManager fm,String tag) {
		DialogFragment waitingDialog =(DialogFragment) fm .findFragmentByTag(tag);
		if (waitingDialog != null) {
			waitingDialog.dismissAllowingStateLoss();
		}
	}
	
	public abstract void onLoadData(boolean bforceUpdate);
	public abstract void onDeleteClick();
	public abstract void onUninstallClick();
	public abstract void onSelAllClick();
	public abstract void onUnSelAllClick();
}
